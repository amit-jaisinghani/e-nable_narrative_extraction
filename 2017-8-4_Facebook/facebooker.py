
# coding: utf-8

# In[2]:


# WORKS #headless is left as a non-functioning reminder.  See headless facebooker.ipynb
from IPython.display import display, HTML, clear_output
from time import sleep


def startBrowser(headless=False):
    #TODO don't make B and GO global.  instead, return B use .visit
    
    if headless:
        print 'starting headless browser...'
    global B, GO 
    from splinter import Browser
    B=Browser('chrome',headless=headless) #firefox by default, which gives blank screen
    FIND=B.find_by_css
    
def GO(url):
    print 'Going to', url
    B.visit(url)
    
#startBrowser()

from time import sleep

#login
def login():
    print('logging in...')
    GO('http://facebook.com')
    sleep(1)
    B.find_by_id('email').fill('jonschull')
    sleep(1)
    B.find_by_id('pass').fill('7Mca8khD')
    sleep(1)
    Btn=B.find_by_id('loginbutton')
    sleep(1)
    Btn.click()
    sleep(1)
#login()

def init(headless=False):
    startBrowser(headless)
    sleep(4)
    login()

#init()
    
#init()
#init(headless=True)
#init(True)

#init(headless= True)
#sleep(3) #allow time to click "allow"
#display(HTML(B.html))


# In[2]:


#startBrowser()
#login()


# In[3]:


#B.visit('https://www.facebook.com/groups/enablecollaborators/')


# In[3]:


#storeRestore UTILITY 
# with backup 7/26/17
# with returnData 8/7/17

#doesn't work as a module
import dill as pickle
def save(filename, data):
    open(filename,'w').write(pickle.dumps(data))
    print 'Saved:', filename,  'Length=', len(data)

def retrieve(varname):
    return  pickle.loads( open(varname).read() )

def restore(varname='testVar', returnData=False):
    """give me the name of  a variable in the global namespace
        and I'll give it the contents of varname.DAT
    """
    globals()[varname] = retrieve(varname+'.DAT')
    print varname+'.DAT', 'retrieved.  Length=', len(globals()[varname])
    if returnData:
        return globals()[varname]
    

    
from dateparser import parse
def ts(): #timestamp
    return '{:%Y%m%d.%H%M.%f}'.format(parse('now'))

def ts(): #timestamp
    return '{:%Y%m%d.%H%M.%f}'.format(parse('now'))

def backupSuffix():
    return ts()
    #return str(parse('now')).split('.')[0] 

from os.path import exists
from os import rename

def store(varname= 'testVar', backup=True):
    """give the name of the variable
       and I'll save it's context in a file called varname.DAT"""
    
    fname = varname+'.DAT'
    if backup and exists(fname):
        newName = fname + ' ' + backupSuffix()
        rename(fname, newName)
        print 'Backing up', newName + ';  ',
    else:
        print 'saving but not backing up...',
    save(fname, globals()[varname])    
    
    
    
mytest2='my test'  #note this works for GLOBAL variables
def test():
    store('mytest2')
    restore('mytest2')
    print '==='+mytest2+'==='
    store('mytest2',backup=True)

test()


# In[4]:


#fill the brower with posts

#does not store anything
#these two are used by scanAndUpdate()

#startBrowser()
#login()

import os

def getAllOf(url='https://www.facebook.com/eNableUkraine/'):
    "Fill Browser by scrolling to the bottom until end"
    from time import sleep
    GO(url)
    lastLength=-1
    lenBh=0
    while lenBh > lastLength:
        if os.path.isfile('STOP'): 
            print 'STOPFILE FOUND\ndelete the file named "STOP" and re-run'
            break
        else:
            lastLength = lenBh
            lenBh = len(B.html)
            print lenBh,
            B.execute_script("window.scrollBy(0,20000)")
            sleep(4) #allow page to load.  HOW LONG IS LONG ENOUGH?
            len(B.html)
    print 'end of page'
    return B.find_by_css('.mbm')

    
#getAllOf()
#('https://www.facebook.com/LimbitlessSolutions')


# In[ ]:


import json
import attrdict
from pymongo import MongoClient
from datetime import datetime

def insertIntoDB(ID, sourceURL, img, chapterName, timeString, date, html, retrievedFrom, retrievedDate):
    client = MongoClient('127.0.0.1', 27017)
    db = client.faceBookDB
    faceBookPosts = db.faceBookPosts
    postInfo = {"id" : ID, "sourceURL": sourceURL, "img" : img,  "chapterName" : chapterName,                 "timeString" : timeString, "date" : date, "html" : html,                 "retrievedFrom" : retrievedFrom, "retrievedDate" : retrievedDate}
    faceBookPosts.insert_one(postInfo)
    
    
def convertToDateTimeISOFormat(dateTimeString):
    isoFormatDateTimeString = datetime.strptime(dateTimeString, '%Y-%m-%d').isoformat() + ".000Z"
    isoFormatDateTime = datetime.strptime(isoFormatDateTimeString, "%Y-%m-%dT%H:%M:%S.000Z")
    return isoFormatDateTime


# In[5]:


def getFBsourceURLs():
    """get the list and then remove the big thang"""
    
    if exists('FBsourceURLs'):
        ret = restore('FBsourceURLs', returnData=True)
        return ret
    
    restore('faceBookPosts')
    ret = [p.sourceURL for p in faceBookPosts]
    del globals()['faceBookPosts']
    return ret
    
#getFBsourceURLs()[-50:]


# In[6]:




def postsToPostRecords(posts, resumeAt=0):   #resume at for debugging
    """
    assumes browser is full
    Turn a browser full of facebook posts into a bunch of post-records
    reject problematic ones
    returns new information (does not store)

    Makes FBsourceURLs global

    """
    global FBsourceURLs
    
    #posts =  posts = B.find_by_css('.mbm')
    print '\npostsToPostRecords:', len(posts), 'posts to go'  
    #After loading the entire Feed Page (and whle the Browser is still alive)
    #parse and store all the new, nonn hcref posts that are resident in the browser.
    from attrdict import AttrDict
    from dateparser import parse
    from IPython.display import display, HTML, clear_output

    #get sourceURLs, maxID
    FBsourceURLs = getFBsourceURLs()

    newNeedToPost = [] #(for ths page)
    newFaceBookPosts = []
    for i,post in enumerate(posts[resumeAt:]): #this process could bail when we lose hope
        print resumeAt + i,
        if os.path.isfile('STOP'): 
            print 'STOPFILE FOUND\ndelete the file named "STOP" and re-run'
            break
        else:
            anchors = [a for a in post.find_by_tag('a') if a.text]
            p=AttrDict()
            p.reject=False
            hasSourceURL=False
            
            if len(anchors)>2: 
                p.sourceURL = anchors[1]['href']
                hasSourceURL=True
                print p.sourceURL,

            else:
                p.reject=True
                print 'Rejected because no good href'
                display(HTML(post.html))
            
            if hasSourceURL:
                
                if p.sourceURL in FBsourceURLs:
                    print 'Rejected because already in FBsourceURLs'
                    p.reject = True
                else:
                    if 'hc_ref=' not in p.sourceURL: #these are different every time. So don't bother saving them.
                        FBsourceURLs.append(p.sourceURL)
                        #store them even if they are subsequently rejected
                        
                if '?hc_ref=' in p.sourceURL:
                    print 'Rejected because ?hcr_ref='; p.reject = True
                if 'instagram' in p.sourceURL:
                    print 'Rejected because instagram'; p.reject = True
                if '/events' in p.sourceURL: #this may be worth revisiting
                    print 'Rejected because event';     p.reject = True

                if not p.reject:
                    if post.find_by_tag('img'):
                        p.img = post.find_by_tag('img')[0]['src']
                    else:
                        p.img ='NoImage'

                    p.chapterName = anchors[0].text
                    p.timeString=post.find_by_css('.timestampContent').text
                    p.date = parse(p.timeString).date()
                    p.html = post.html #not tested
                    p.retrievedFrom = B.url
                    p.retrievedDate = parse('now').date
                    p.ID = ts() #timestamp

                    insertIntoDB(p.ID, p.sourceURL, p.img, p.chapterName, p.timeString, \
                    		convertToDateTimeISOFormat(str(p.date)), p.html, p.retrievedFrom,     \
                    		convertToDateTimeISOFormat(str(p.retrievedDate())))     
                    
                    print 'NEW sourceURL', p.sourceURL
                    newNeedToPost.append(p)
                    FBsourceURLs.append(p.sourceURL) #just for internal use 
                    newFaceBookPosts.append(p)

    store('FBsourceURLs') 
    print 'Returning', len(newNeedToPost), 'pageRecords'
    return newNeedToPost, newFaceBookPosts, FBsourceURLs
    


# In[7]:


def test():
   #init(headless=False)
    posts = getAllOf('https://www.facebook.com/enable.all/pages_feed/' )
    newNeedToPost, newFaceBookPosts, FBsourceURLs = postsToPostRecords(posts) #restores global faceBookPosts
#test()


# In[10]:


def addToStore(varName='faceBookPosts', newData=[]):
    """ update the database and then clear it from memory
    """
    restore(varName)
    globals()[varName]  += newData
    store(varName)
    del globals()[varName]
    

def scanAndUpdate( URL='https://www.facebook.com/enable.all/pages_feed/'):
    """
    For a single URL,
    scan All of that URL's posts,
    update newNeedToPost and newFaceBookPosts
    then backs up and extends needToPost.DAT and faceBookPosts.DAT
    
    What's the diff between needToPost.DAT and faceBookPosts.DAT?
    When we post these things to the wall, we will remove them from needToPost.DAT but leave them in the comprehensive faceBookPosts.DAT
    """
    global needToPost, faceBookPosts
    posts = getAllOf( URL ) 
    
    newNeedToPost, newFaceBookPosts, FBsourceURLs = postsToPostRecords(posts) #restores global faceBookPosts
    
    print 'newNeedToPost, newFaceBookPosts:', len(newNeedToPost), len(newFaceBookPosts)
    if newNeedToPost:
        addToStore('needToPost', newNeedToPost)

    if newFaceBookPosts:
        addToStore('faceBookPosts', newFaceBookPosts)
        

init()
scanAndUpdate()


# In[20]:


1/0
#!pip install attrdict


# In[ ]:
'''

from attrdict import AttrDict
from unidecode import unidecode

def allPagesLikedByEnableAll(headless=True):
    print 'getting allPagesLikedByEnableAll  headless:', headless
    startBrowser(headless=True)
    login()
    GO('https://www.facebook.com/browse/fanned_pages/?id=365400353862239&showauxiliary=1&fanorigin=timeline_like_chaining&av=365400353862239&ref=page_internal')
    allPagesLiked = []
    items = B.find_by_css('.fsl')
    names = [item.find_by_tag('a').text for item in items]
    names = [unidecode(name) for name in names]
    hrefs = [item.find_by_tag('a')['href'] for item in items]
    hrefs = [href.split('?')[0] for href in hrefs] #chop cruft from end
    #print len(names), len(hrefs)
    
    ret = []
    for i,name in enumerate(names):
        if name.startswith('Haifa3D'):
            names[i]='Haifa3D'# chop out hebrew
        ret.append(AttrDict(name=names[i], href=hrefs[i]))
    return ret

def test():
    allPages = allPagesLikedByEnableAll()
    for i,p in enumerate(allPages):
        print p
#test()


# In[ ]:


#deepScan Newly-liked pages
# i.e., work back to it's earliest posts

def deepScanNewlyLikedPages():

    newAllPages = allPagesLikedByEnableAll() #get from facebook

    restore('allPages') #get from drive

    newHrefs = set([p.href for p in newAllPages])
    print 'new', len(newHrefs)

    oldHrefs = set([p.href for p in allPages])
    print 'old', len(oldHrefs)

    needToDeepScan = newHrefs.difference(oldHrefs)

    if needToDeepScan: 
        print 'need to Deep Scan', len(needToDeepScan), 'new pages'
        for href in needToDeepScan:
            scanAndUpdate(href)

        pagesToAddAllPages = [p for p in newAllPages if p.href in needToDeepScan]
        allPages = allPages + pagesToAddAllPages
        store('allPages')

#deepScanNewlyLikedPages()


# In[ ]:


#prototype command line programs
#not tested

def CMD_scanAndUpdateFeed():
    """
    usage: >facebooker scanAndUpdateFeed 
    """
    init()
    scanAndUpdate('https://www.facebook.com/enable.all/pages_feed/')
    
def CMD_doTheDaily()
    init()
    scanAndUpdate('https://www.facebook.com/enable.all/pages_feed/')
    deepScanNewlyLikedPages()



# In[ ]:


#DEEP SCAN OF ALL, from clean slate
#global variables:
#  faceBookPosts, needToPost, allPages, B for Browser

def comprehensiveScan():

    init(headless=True)

    from os.path import exists
    if not exists('faceBookPosts.DAT'):
        print 'CREATING NEW facebookPosts.DAT and needToPost.DAT'
        faceBookPosts=[]
        store('faceBookPosts')
        needToPost=[]
        store('needToPost')

    if not 'B' in globals().keys(): #start the Browser if it doesn't exist
        init(headless=True)

    if not exists('allPages.DAT'): #remove allPages when you add a page to the feed
        print 'creating allPages'
        allPages = allPagesLikedByEnableAll()
        store('allPages')
    else:
        restore('allPages')

    for i, href in enumerate([page.href for page in allPages][56:]):  #[5:]TEMP PICK UP WHERE LEFT OFF
        print '-----------'
        scanAndUpdate(href)


# In[ ]:


#print all the posts on facebook.all
def printAllPosts():
    """and return a list of urls"""
    from time import sleep
    GO('https://www.facebook.com/enable.all/insights/?section=navPosts')
    while B.find_by_text('See More'):
        B.find_by_text('See More').click()
        sleep(1)
    rows = B.find_by_tag('table').find_by_tag('tr')
    urls = [row[2] ]
    print rows[1].text
    
#printAllPosts()


# In[ ]:


#one-off

def removePostsThatWereTooEarly():
    restore('needToPost')
    global needToPost
    removed = [p for p in needToPost if p.date > datetime.date(2013,7,1)]
    len(removed), len(needToPost)
    needToPost = removed
    store('needToPost')

#removePostsThatWereTooEarly()


# In[ ]:


#post to facebook wall
#WORK IN PROGRESS
#note: needToPost contains many early posts that are not about facebook
#go for it anyway

def postToWall():

    stopAt = 10 #00000  #large number = don't stop
    #NOT CURRENTLY USING stopAT

    startBrowser(headless=False)
    login()
    restore('needToPost')

    from operator import itemgetter
    from time import sleep
    needToPost.sort(key = itemgetter('date'))  #sort by date
    #[p.date for p in faceBookPosts]

    GO('https://www.facebook.com/enable.all/')


    failed=[]
    i=1
    while needToPost:
        if os.path.isfile('STOP'): 
            print 'STOPFILE FOUND.'
            print 'delete the file named "STOP" and re-run'
            break
        else:
            #try:
            p=needToPost[-1]#post most recent first
            i+=1
            sleep(2)
            #activate the text area
            B.execute_script("window.scrollBy(0,-20000)")#make sure the Write Something Field is in view
            d=B.find_by_css('._1hib') #the 'Write something...' field
            sleep(1)
            try:
                d.click()
                sleep(2)
                # click on the editor and load the URL to share
                # note leading and trailing spaces to make the URL 'take'
                nt=B.find_by_css('.notranslate')
                nt.fill(' '+p.sourceURL)
                print p.ID, p.date, p.sourceURL,
                sleep(4) #give it time to load
                #erase the URL now that it's loaded
                B.find_by_css('.notranslate').fill(' ' + str(p.date) )
                sleep(1)
                publish= B.find_by_css('._1mf7') 
                sleep(1)
                publish.click()
                print i,'done'
                needToPost.pop() #remove one just posted
                store('needToPost',backup=False)
                if i%20 == 0:  #restart browser to keep things from 
                    B.quit()
                    startBrowser()
                    login()
                    GO('https://www.facebook.com/enable.all/')
                    #slows down when browser is too full ?

            except AttributeError:
                msg = 'There was a problem updating your status. Please try again in a few minutes.'
                msgs = [d.value for d in B.find_by_css('._50f4')]
                if msg in msgs:
                    print msg
                else:
                    print 'unknown error'
            
            sleep(2)

    print ">>>>Finished?", failed
    
postToWall()


# In[ ]:


1/0 #stop here


# In[ ]:


display(HTML(B.html))


# In[ ]:


#####
#G+ Poster 
startBrowser(headless=False)
import time
def Glogin():
    GO('https://accounts.google.com/ServiceLogin?passive=1209600&osid=1&continue=https://plus.google.com/collections/featured&followup=https://plus.google.com/collections/featured#identifier')
    B.find_by_id('identifierId').fill('jschull@gmail.com')
    B.find_by_id('identifierNext').click()
    time.sleep(2)
    x=B.find_by_name('password').fill('remadepassword')
    x=B.find_by_text('Next').click()

    
Glogin()
time.sleep(2)
GO('https://plus.google.com/u/0/communities/102497715636887179986/stream/642d377d-4b51-43b9-be68-18a4a7afa261')
print B.html[:800]


# In[ ]:


B.url


# In[ ]:


from bs4 import BeautifulSoup
soup = BeautifulSoup(p.html)
soup.get_text()


# In[ ]:


things = [thing.text for thing in soup.find_all(['p','span'])]
for thing in things:
    print thing


# In[ ]:


restore('needToPostGplus')
urls=[p.sourceURL for p in needToPostGplus]
len(set(urls))


# In[ ]:


#G+ Poster.  
#restore('needToPostGplus')
#from operator import itemgetter
#needToPostGplus.sort(key = itemgetter('date'))
#startBrowser()
#Glogin()
#time.sleep(3)

import os
while needToPostGplus:
    if os.path.isfile('STOP'): 
        print 'STOPFILE FOUND.'
        print 'delete the file named "STOP" and re-run'
        break
    else:

        p=needToPostGplus[-1]
        url = 'https://plus.google.com/share?url=' + p.sourceURL
        B.visit(url)
        time.sleep(3)

        ta = B.find_by_css('textArea')[0]
        ta.fill('/posted to facebook ' + str(p.date) + '/')

        time.sleep(1)

        ts = [t for t in B.find_by_tag('div') if t.value=='POST']
        ts[-1].click()
        categories = B.find_by_css('.N9wOvf.Jxhqvd')
        time.sleep(1)
        clickhere = [item for item in categories if item.value=='From Affiliated Sites'][0]
        clickhere.click()
        needToPostGplus.pop()
        store('needToPostGplus', backup=False)
        time.sleep(3)
    
    


# In[ ]:


B.visit('https://plus.google.com/share?url=https://www.facebook.com/enablemadisonwisconsin/posts/1839550469703448')
time.sleep(3)

B.find_by_css('.O0WRkf.zZhnYe.e3Duub.C0oVfc.M9Bg4d').click()

categories = B.find_by_css('.N9wOvf.Jxhqvd')
time.sleep(1)
clickhere = [item for item in categories if item.value=='From Affiliated Sites'][0]
clickhere.click()


# In[ ]:


B.visit('https://plus.google.com/share?url=https://www.facebook.com/enablemadisonwisconsin/posts/1839550469703448')


# In[ ]:


#G+ POST

from bs4 import BeautifulSoup

#restore('needToPostGplus')
p=needToPostGplus[-8]

soup = BeautifulSoup(p.html)
soup.get_text()

postBox = B.find_by_css('.qmdUde')
postBox[0].click()

textArea = B.find_by_id('XPxXbf')
time.sleep(1)
textArea.click()

#content = '\t\t\n' + p.sourceURL + '\n' #set the link

#content = '/From facebook ' + str(p.date) + '/\n'
#content += p.sourceURL + '\n\n'
#content += '\n\n'.join([para.text for para in soup.find_all('p')]) 

#content = '\t \t \t \t \t \t \t \t \t' + '\n\n\n' #tab to get to Post; 
#textArea.fill(content)

time.sleep(2)
categories = B.find_by_css('.N9wOvf.Jxhqvd')
clickhere = [item for item in categories if item.value=='From Affiliated Sites'][0]
clickhere.click()


# In[ ]:


b = B.find_by_css('.mUbCce.fKz7Od.M9Bg4d')[0]


# In[ ]:


b.click()
get_ipython().magic(u'pinfo b.action_chains')


# In[ ]:


#postButton = [pb for pb in B.find_by_css('.O0WRkf') if pb.value=='POST'][0]
B.find_by_text('Post').click()


# In[ ]:


from IPython.display import display, HTML, clear_output

def searchPosts(search = 'Jen Owen'):
    for p in faceBookPosts:
        if 'html' in p.keys():
            if search in p.html:
                print display(HTML(p.html + '<hr/>'))
                
searchPosts()


# In[ ]:


s='abc'
s+='def'
s
'''
