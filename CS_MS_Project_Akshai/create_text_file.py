import csv


def read(csvfile, lines):
    with open(csvfile) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
                lines.append(row)


def read_file():
    lines_akshai = []
    read('Google_Posts_with__frames_marked.csv', lines_akshai)

    lines_jon = []
    read('jon_labels.csv', lines_jon)

    lines_homan = []
    read('homan_labels.csv', lines_homan)

    return lines_akshai, lines_jon, lines_homan


def convert(file_lines, name):
    lines = []
    count = 0
    for line in file_lines:
        new_line = ''
        for i in range(2, 9):
            if i < 8:
                if line[i] == '':
                    new_line += '0' + '\t'
                else:
                    new_line += line[i] + '\t'
            else:
                if line[i] == '':
                    new_line += '0' + '\n'
                else:
                    new_line += line[i] + '\n'

        lines.append(new_line)
        count += 1
        if count == 200:
            break


    f = open(name, 'w')
    for line in lines:
        f.write(line)
    f.close()

def create_text_from_csv():

    lines_akshai, lines_jon, lines_homan = read_file()

    convert(lines_akshai, 'akshai_labels.txt')

    convert(lines_homan, 'chris_labels.txt')

    convert(lines_jon, 'jon_labels.txt')


create_text_from_csv()



