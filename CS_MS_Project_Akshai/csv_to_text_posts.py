from pymongo import MongoClient
import csv


def get_db_connection():
    client = MongoClient('localhost:27017')

    db = client.enable

    posts = db.Post.find()
    comments = db.Comment.find()
    print 'Total posts:', posts.count()
    return posts, comments


def write_to_csv(contents, filename):
    with open(filename, 'w') as fd:
        writer = csv.writer(fd)
        for content in contents:
            writer.writerow(content)
def write_to_json(contents, filename):
    import json
    with open(, 'w') as outfile:
        json.dump(data, outfile)

def decodeToUTFEight(content):
    try:
        return content.encode('utf-8').decode()
    except UnicodeDecodeError:
        return content.encode('utf-8')


def read_from_db():
    posts, comments = get_db_connection()
    post_contents = []
    comment_contents = []
    for post in posts:
        print post['content']
        post_contents.append(post['content'])
        print '------'
    for comment in comments:
        print comment['comment']
        comment_contents.append(comment['comment'])
        print '------'

    write_to_csv(post_contents, 'post_contents.json')
    write_to_csv(comment_contents, 'comment_contents.json')


read_from_db()
