def exact_change(ones, fives, amount):
    '''
    if ones + (fives * 5) >= amount:
        number_of_fives_to_use = amount // 5
        number_of_ones_to_use = amount % 5

        if amount - (5 * number_of_fives_to_use) <= ones:
                return True

        elif amount - (5 * number_of_fives_to_use) <= 0:
            if amount % 5 == 0:
                return True
            else:
                if ones >= number_of_ones_to_use:
                    return True
        else:
            return True
    '''

    if amount % 5 > ones:
        return False
    if amount - (5 * fives) > ones:
        return False

    return True

