import json
import datetime
import csv
import time
import os

import pandas as pd
import sys, getopt, pprint
from pymongo import MongoClient

try:
    from urllib.request import urlopen, Request
except ImportError:
    from urllib2 import urlopen, Request

app_id = "1657158624346860"
app_secret = "01c69a23aa5e9f53ae3e39657c636bb5"
# page_id = "teamunlimbited"

start_date = "2012-01-01"
end_date = "2017-10-01"

access_token = app_id + "|" + app_secret

'''
    Retry URL multiple time till 200 success message is received
'''


def retryMultipleTimes(url):
    request = Request(url)
    isSuccess = False
    count = 0
    while isSuccess is False:
        try:
            response = urlopen(request)
            if response.getcode() == 200:
                isSuccess = True
        except Exception as e:
            count += 1
            print(e)
            time.sleep(5)

            print('Error at URL: ', url)
            print('Retrying...')

            if (count == 5):
                return None

    return response.read()


'''
    Convert to UTF, standard type to store
'''


def decodeToUTFEight(content):
    try:
        return content.encode('utf-8').decode()
    except UnicodeDecodeError:
        return content.encode('utf-8')


'''
    Generates the page URL and returns it
'''


def getPageUrl(url):
    url_suffix = '&fields=message,link,created_time,type,name,id,' + \
                 'comments.limit(0).summary(true),shares,reactions' + \
                 '.limit(0).summary(true)'

    return url + url_suffix


'''
    Generates the comment URL and returns it
'''


def getCommentUrl(base_url):
    fields = "&fields=id,message,reactions.limit(0).summary(true)" + \
             ",created_time,comments,from,attachment"
    url = base_url + fields

    return url


'''
    Generates after URL suffix required for processing posts and comments
'''


def getAfterURL(after_url):
    if after_url is not '':
        after_url = '&after=' + after_url

    return after_url


'''
    For a given post or comment URL returns all the (emoticons)
'''


def getEmoticons(url):
    emoticons = ['LIKE', 'WOW', 'LOVE', 'HAHA', 'SAD', 'ANGRY']
    emoticons_map = {}

    for emotion in emoticons:
        url_suffix = '&fields=reactions.type(' + emotion + ').limit(0).summary(total_count)'

        url = url + url_suffix

        response = retryMultipleTimes(url)
        if response is not None:
            records = json.loads(response)['data']
        else:
            return response

        records_added = set()
        for record in records:
            record_id = record['id']
            count = record['reactions']['summary']['total_count']
            records_added.add((record_id, count))

        for record_id, count in records_added:
            if record_id not in emoticons_map:
                emoticons_map[record_id] = (count,)
            else:
                emoticons_map[record_id] = emoticons_map[record_id] + (count,)

    return emoticons_map


'''
    Return post or comment date in ISO format EST time 
'''


def getDate(record):
    recrod_date = datetime.datetime.strptime(record['created_time'], '%Y-%m-%dT%H:%M:%S+0000')
    recrod_date = recrod_date + datetime.timedelta(hours=-5)
    recrod_date = recrod_date.strftime('%Y-%m-%d %H:%M:%S')

    return recrod_date


'''
    Post processor, that extracts the information in a post using its metadata
'''


def processPosts(page_id, post):
    post_id = post['id']
    post_type = post['type']

    post_content = ''
    if 'message' in post:
        post_content = decodeToUTFEight(post['message'])

    post_user = ''
    if 'name' in post:
        post_user = decodeToUTFEight(post['name'])

    post_link = ''
    if 'link' in post:
        post_link = decodeToUTFEight(post['link'])

    post_date = getDate(post)

    comment_count = 0
    if 'comments' in post:
        comment_count = post['comments']['summary']['total_count']

    share_count = 0
    if 'shares' in post:
        share_count = post['shares']['count']

    emoticon_count = 0
    if 'reactions' in post:
        emoticon_count = post['reactions']['summary']['total_count']

    return page_id, post_id, post_content, post_user, post_type, post_link, post_date, emoticon_count, comment_count, share_count


'''
    Comment processor, that extracts the information in a comment using its metadata
'''


def processComments(page_id, comment, post_id, origin_id=''):
    comment_id = comment['id']
    comment_content = '' if 'message' not in comment or comment['message'] \
                                                        is '' else decodeToUTFEight(comment['message'])
    # print comment_content
    commenter = decodeToUTFEight(comment['from']['name'])

    emoticon_count = 0 if 'reactions' not in comment else \
        comment['reactions']['summary']['total_count']

    if 'attachment' in comment:
        attachment_type = comment['attachment']['type']
        attachment_type = 'gif' if attachment_type == 'animated_image_share' \
            else attachment_type
        attachment_tag = "[[{}]]".format(attachment_type.upper())
        comment_content = attachment_tag if comment_content is '' else \
            comment_content + " " + attachment_tag

    comment_date = getDate(comment)

    return page_id, comment_id, post_id, origin_id, comment_content, commenter, comment_date, emoticon_count


'''
    Detects if there is a next page for the post processor to scrape, return false for has_next_page otherwise
'''


def setHasNextPageForPost(has_next_page, after_url, posts):
    if 'paging' in posts:
        after_url = posts['paging']['cursors']['after']
    else:
        has_next_page = False

    return after_url, has_next_page


'''
    Stores posts in a csv file
'''


def storePosts(page_id, access_token):
    temp_page_id = page_id

    if '-' in page_id:
        page_id = page_id.split('-')[-1]
    try:
        post_file = open(temp_page_id + '_facebook_posts.csv', 'w')

    except Exception:
        print 'File not found'

    file_writer = csv.writer(post_file)
    file_writer.writerow(['page_id', 'post_id', 'post_content', 'post_user', 'post_type',
                          'post_link', 'post_date', 'total_emotion_count', 'comment_count',
                          'share_count', 'like_count', 'love_emotion_count',
                          'wow_emotion_count', 'haha_emotion_count', 'sad_emotion_count',
                          'angry_emotion_count', 'special_emotion_count'])

    has_next_page = True

    post_count = 0

    base_url = 'https://graph.facebook.com/v2.9'
    page_id_url = '/' + page_id + '/posts'
    parameters_url = '/?limit=100&access_token=' + access_token
    start_date_url = '&since=' + start_date
    end_date_url = '&until=' + end_date

    print 'Storing posts for: ' + page_id

    after_url = ''

    while has_next_page:

        after_url = getAfterURL(after_url)

        base_url = base_url + page_id_url + parameters_url + after_url + start_date_url + end_date_url

        url = getPageUrl(base_url)
        response = retryMultipleTimes(url)
        if response is not None:
            posts = json.loads(response)
        else:
            return response

        emoticons = getEmoticons(base_url)

        if emoticons is None:
            return None

        for post in posts['data']:

            if 'reactions' in post:
                post_info = processPosts(temp_page_id, post)
                emoticons_info = emoticons[post_info[1]]

                special_count = post_info[7] - sum(emoticons_info)
                file_writer.writerow(post_info + emoticons_info + (special_count,))

            post_count += 1
            # print post_count

        after_url, has_next_page = setHasNextPageForPost(has_next_page, after_url, posts)

    post_file.close()

    print 'For ' + page_id + ': ' + str(post_count) + ' posts stored'


'''
    Detects if there is a next page for the comments processor to scrape, return false for has_next_page otherwise
'''


def setHasNextPageForComments(has_next_page, after_url, list):
    if 'paging' in list:
        if 'next' in list['paging']:
            after_url = list[
                'paging']['cursors']['after']
        else:
            has_next_page = False
    else:
        has_next_page = False

    return after_url, has_next_page


'''
    Writes comments to file    
'''


def writeToFile(page_id, iter, id, arr, file, origin_id=''):
    comment_info = processComments(page_id, iter, id, origin_id)
    emoticon_info = arr[comment_info[1]]

    special_emoticon_count = comment_info[7] - sum(emoticon_info)
    file.writerow(comment_info + emoticon_info + (special_emoticon_count,))


'''
    Stored comments in csv file
'''


def storeComments(page_id, access_token):
    temp_page_id = page_id

    if '-' in page_id:
        page_id = page_id.split('-')[-1]

    try:
        comment_file = open(temp_page_id + '_facebook_comments.csv', 'w')
    except Exception:
        print 'File not found'

    file_writer = csv.writer(comment_file)
    file_writer.writerow(['page_id', 'comment_id', 'post_id', 'origin_id', 'comment_content',
                          'commenter', 'comment_date', 'total_emotion_count',
                          'like_count', 'love_count', 'wow_count', 'haha_count',
                          'sad_count', 'angry_count', 'special_emotion_count'])

    comment_count = 0
    after_url = ''
    base_url = 'https://graph.facebook.com/v2.9'
    parameters = '/?limit=100&access_token=' + access_token

    print 'Storing comments for: ' + page_id

    try:
        post_file = open(temp_page_id + '_facebook_posts.csv', 'r')
    except Exception:
        print 'File not found'

    reader = csv.DictReader(post_file)

    for post in reader:

        has_next_page = True

        while has_next_page:

            comment_url = '/' + post['post_id'] + '/comments'

            after_url = getAfterURL(after_url)

            merged_url = base_url + comment_url + parameters + after_url

            url = getCommentUrl(merged_url)
            # print(url)

            response = retryMultipleTimes(url)
            if response is not None:
                comments = json.loads(response)
            else:
                return response
            emotions = getEmoticons(merged_url)

            for comment in comments['data']:

                writeToFile(temp_page_id, comment, post['post_id'], emotions, file_writer)

                if 'comments' in comment:
                    has_next_page_sub = True
                    after_url_sub = ''

                    while has_next_page_sub:
                        comment_url_sub = '/' + comment['id'] + '/comments'

                        after_url_sub = getAfterURL(after_url_sub)

                        base_url_sub = base_url + comment_url_sub + parameters + after_url_sub

                        url_sub = getCommentUrl(base_url_sub)

                        response = retryMultipleTimes(url_sub)

                        if response is not None:
                            comments_sub = json.loads(response)
                        else:
                            response

                        emotions_sub = getEmoticons(base_url_sub)

                        for comment_sub in comments_sub['data']:
                            writeToFile(temp_page_id, comment_sub, post['post_id'], emotions_sub, file_writer,
                                        comment['id'])

                            comment_count += 1

                        after_url_sub, has_next_page_sub = setHasNextPageForComments(has_next_page_sub,
                                                                                     after_url_sub, comments_sub)

                comment_count += 1

            after_url, has_next_page = setHasNextPageForComments(has_next_page, after_url, comments)

    comment_file.close()
    post_file.close()

    print 'For ' + page_id + ': ' + str(comment_count) + ' comments stored'


def execute():
    page_id = 'enable.all'

    storePosts(page_id, access_token)
    storeComments(page_id, access_token)

    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        if f.endswith(".csv"):
            file = open(f, 'r')
            reader = csv.DictReader(file)
            mongo_client = MongoClient()
            db = mongo_client.faceBook_DB
            faceBookComments = db.faceBook_comments_data
            faceBookPosts = db.faceBook_posts_data
            # print file
            if 'comments' in str(file):
                print str(file)
                for each in reader:
                    # print (each)
                    faceBookComments.update_one({'comment_id': each['comment_id']}, {'$set': each}, upsert=True)

            if 'posts' in str(file):
                print str(file)
                for each in reader:
                    # print (each)
                    faceBookPosts.update_one({'post_id': each['post_id']}, {'$set': each}, upsert=True)
        file.close()


if __name__ == '__main__':
    execute()
