from pymongo import MongoClient
from matplotlib import pyplot as plt
import numpy as np
import nltk
from nltk.corpus import wordnet


def get_db_connection():
    client = MongoClient('localhost:27017')

    db = client.enable

    posts = db.Post.find()
    print 'Total posts:', posts.count()
    return posts, db


def decodeToUTFEight(content):
    try:
        return content.encode('utf-8').decode()
    except UnicodeDecodeError:
        return content.encode('utf-8')


def result_for_frame(posts, keywords):
    result_posts = []
    for post in posts:
        try:
            # print posts['post_content']
            for keyword in keywords:
                if keyword.lower() in post['content'].lower() \
                        and post not in result_posts:
                    # print post['post_content']
                    # print decoded_post_content
                    result_posts.append(post)
        except Exception as e:
            #print e
            continue

    return result_posts


def insert_into_db(result_posts, frames):
    for result_post in result_posts:
        this_post = frames.find_one({'url': result_post['url']})
        if this_post is None:
            frames.insert_one(result_post)

def report_frame_analysis():

    report_keywords = ['report', 'summarize', 'summary', 'article'
                       'sum up', 'outline', 'boiled down', 'draft'
                       'synopsis', 'brief', 'describe',
                       'detail', 'communicate', 'result',
                       'designed', 'discuss', 'submit', 'aim',
                       'objective', 'demo', 'document', 'describe'
                       'review', 'release', 'overview', 'efficiency'
                       'benefits', 'conclusion', 'proved', 'final']
    use_keywords = []
    for word in report_keywords:
        for syn in wordnet.synsets(word):
            for lemma in syn.lemmas():
                use_keywords.append(lemma)

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    result_posts = result_for_frame(posts, report_keywords)

    report_frames = db.report_frame_data

    #insert_into_db(result_posts, report_frames)

    print 'Report frame:', len(result_posts)
    return len(result_posts)


def device_frame_analysis():

    device_report_keywords = ['device', 'arm', 'hand', 'limb', 'gadget', 'thumb', 'wrist',
                              'prosthetic', 'prosthesis', 'prostheses',
                              'unlimbited', 'cyborg', 'raptor', 'limbforge',
                              'design', 'beast', 'k1', 'r1', 'oogoo']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    device_result_posts = result_for_frame(posts, device_report_keywords)

    device_frames = db.device_frame_data

    insert_into_db(device_result_posts, device_frames)

    print 'Device frame:', len(device_result_posts)
    return len(device_result_posts)


def delivery_frame_analysis():

    delivery_result_frame = ['delivery', 'deliverer', 'send', 'sent', 'conveyance',
                             'receive', 'transport', 'consignment', 'shipment', 'shipped',
                             'distributed', 'sent', 'got', 'gets', 'gave', 'is here',
                             'loves', 'loved', 'very happy', 'is happy', 'provide',
                             'mail', 'showing off', 'tryout']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    delivery_result_posts = result_for_frame(posts, delivery_result_frame)

    delivery_frames = db.delivery_frame_data

    insert_into_db(delivery_result_posts, delivery_frames)

    print 'Delivery frame:', len(delivery_result_posts)
    return len(delivery_result_posts)


def progress_frame_analysis():

    progress_result_frame = ['advance', 'burgeon', 'develop', 'maturation', 'making',
                             'progress', 'stagnate', 'improve', 'on track', 'added',
                             'printed', 'printing', 'working', 'build', 'built', 'update'
                             'ready', 'complete', 'prepare', 'preparing', 'checkout',
                             'version', 'process', 'developing', 'change', 'removed', 'running',
                             'creating', 'fixed', 'produce', 'finish', 'derived', 'training',
                             'test', 'experiment', 'experimenting', 'so far', 'investigate',
                             'made', 'almost done', 'inspection', 'plan', 'restore',
                             'about to', 'modified', 'modify', 'result', 'investigate',
                             'assemble', 'increase', 'yet', 'broke', 'fail', 'success',
                             'stage', 'iteration', 'step further', 'vision', 'starting']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    progress_result_posts = result_for_frame(posts, progress_result_frame)

    progress_frames = db.progress_frame_data

    insert_into_db(progress_result_posts, progress_frames)

    print 'Progress frame:', len(progress_result_posts)
    return len(progress_result_posts)


def becoming_member_frame_analysis():

    becoming_member_result_frame = ['enlist', 'enroll', 'enter', 'join',
                                    'sign up', 'register', 'volunteer',
                                    'get started', 'hi ', 'hi,', 'hi!',
                                    'hello everyone', 'hello', 'hey',
                                    'fabricator', 'introduce',  'to help',
                                    'newcomer', 'measured', 'support',
                                    'get involved', 'getting involved',
                                    'my name', 'newbie', 'fabricator',
                                    'start contributing', 'new member',
                                    'new here', 'adding me', 'accepting']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    becoming_member_result_posts = result_for_frame(posts, becoming_member_result_frame)

    becoming_member_frames = db.becoming_member_frame_data

    insert_into_db(becoming_member_result_posts, becoming_member_frames)

    print 'Becoming member frame:', len(becoming_member_result_posts)
    return len(becoming_member_result_posts)


def attempt_action_frame_analysis():

    attempt_action_result_frame = ['attempt', 'effort', 'endeavor', 'try',
                                   'undertake', 'go', 'push', 'need', 'trying', 'look at',
                                   'looking for', 'looking to', 'born with', 'lost a', 'like to',
                                   'request', 'tried', 'call for', 'calling out', 'love to']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    attempt_action_result_posts = result_for_frame(posts, attempt_action_result_frame)

    attempt_action_frames = db.attempt_action_frame_data

    insert_into_db(attempt_action_result_posts, attempt_action_frames)

    print 'Attempt action frame: ', len(attempt_action_result_posts)
    return len(attempt_action_result_posts)


def activity_frame_analysis():

    activity_result_frame = ['begin', 'commence', 'initiate', 'get started',
                             'open', 'start', 'abandon', 'quit', 'halt',
                             'terminate', 'stop', 'discontinue', 'done', 'conference',
                             'finished', 'through', 'complete', 'conclude', 'event',
                             'wrap up', 'planning', 'meeting', 'workshop', 'seminar',
                             'visited', 'activity', 'exhibit', 'news', 'media', 'contest']

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    activity_result_posts = result_for_frame(posts, activity_result_frame)

    activity_frames = db.activity_frame_data

    insert_into_db(activity_result_posts, activity_frames)

    print 'Activity frame:', len(activity_result_posts)
    return len(activity_result_posts)


def other_frame():
    posts, db = get_db_connection()

    report_frames = db.report_frame_data
    progress_frames = db.progress_frame_data
    device_frames = db.device_frame_data
    delivery_frames = db.delivery_frame_data
    becoming_member_frames = db.becoming_member_frame_data
    attempt_action_frames = db.attempt_action_frame_data
    activity_frames = db.activity_frame_data
    other_frame = db.other_frame_data

    other_frame_result = []
    for post in posts:
        if report_frames.find_one({'url': post['url']}) is None \
                and progress_frames.find_one({'url': post['url']}) is None \
                and device_frames.find_one({'url': post['url']}) is None \
                and delivery_frames.find_one({'url': post['url']}) is None \
                and becoming_member_frames.find_one({'url': post['url']}) is None \
                and attempt_action_frames.find_one({'url': post['url']}) is None \
                and activity_frames.find_one({'url': post['url']}) is None:
            other_frame_result.append(post)

    insert_into_db(other_frame_result, other_frame)
    print 'Other frame:', len(other_frame_result)
    return len(other_frame_result)

def draw_graph(x_list, y_list):

    index = np.arange(len(x_list))
    bar_width = 0.35

    opacity = 0.4

    plt.bar(index, y_list, bar_width,
                     alpha=opacity,
                     color='b')
    #plt.figure('Frame Distribution for Google+ data', (10, 10))
    plt.title('Frame Distribution for Google+ data')
    #plt.plot([i for i in range(len(y_list))], y_list, 'b-')
    plt.xticks(index, x_list, rotation=60)
    plt.xlabel('Frame')
    plt.ylabel('Post Count')
    plt.legend()

    plt.tight_layout()
    #plt.show()
    plt.savefig('Frame Distribution for Google+ data.pdf')


def perform_analysis():
    y_list = []
    y_list.append(becoming_member_frame_analysis())
    y_list.append(device_frame_analysis())
    y_list.append(attempt_action_frame_analysis())
    y_list.append(progress_frame_analysis())
    y_list.append(activity_frame_analysis())
    y_list.append(delivery_frame_analysis())
    y_list.append(report_frame_analysis())
    y_list.append(other_frame())

    x_list = ['Becoming_member', 'Device', 'Attempt_action', 'Progress',
              'Activity_frame', 'Delivery', 'Report', 'Other']
    # draw_graph(x_list, y_list)
    return x_list, y_list


def main():
    perform_analysis()
    

if __name__ == '__main__':
    main()
