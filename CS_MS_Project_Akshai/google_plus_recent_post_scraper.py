'''
import time

from selenium import webdriver
from splinter import Browser

def startBrowser(headless=False):
    #TODO don't make B and GO global.  instead, return B use .visit

    if headless:
        print 'starting headless browser...'
    global B, GO
    B=Browser('chrome',headless=headless) #firefox by default, which gives blank screen
    FIND=B.find_by_css

#executable_path = {'executable_path':'C:\Users\Akshai\Downloads\chromedriver.exe'}

executable_path = {'executable_path':'/usr/bin/chromedriver'}

B = Browser('chrome', **executable_path) #firefox by default
FIND=B.find_by_css
GO=B.visit

def login():
    GO()
    #GO('https://plus.google.com/u/0/communities/102497715636887179986')
    B.find_by_id('identifierId').fill('akshai.jb007@gmail.com')
    B.find_by_id('identifierNext').click()
    time.sleep(2)
    time.sleep(2)
    B.find_by_id('passwordNext').click()

def init(headless=False):
    startBrowser(headless)
    sleep(4)
    login()
    sleep(4)
    GO('https://plus.google.com/u/0/communities/102497715636887179986')

init()
'''
from IPython.display import display, HTML, clear_output
from time import sleep


def startBrowser(headless=False):
    #TODO don't make B and GO global.  instead, return B use .visit

    if headless:
        print 'starting headless browser...'
    global B, GO
    from splinter import Browser
    B=Browser('chrome',headless=headless) #firefox by default, which gives blank screen
    FIND=B.find_by_css

def GO(url):
    print 'Going to', url
    B.visit(url)

#startBrowser()

from time import sleep

#login
def login():
    print('logging in...')
    GO('https://accounts.google.com/ServiceLogin?passive=1209600&osid=1&continue=https://plus.google.com/collections/featured&followup=https://plus.google.com/collections/featured#identifier')
    B.find_by_id('identifierId').fill('akshai.jb007')
    B.find_by_id('identifierNext').click()
    sleep(2)
    B.find_by_name('password').fill('11nov!992')
    sleep(2)
    B.find_by_id('passwordNext').click()
#login()

def init(headless=False):
    startBrowser(headless)
    login()
    sleep(2)
    GO('https://plus.google.com/u/0/communities/102497715636887179986')

init()


def unicode_decode(text):
    try:
        return text.encode('utf-8').decode()
    except UnicodeDecodeError:
        return text.encode('utf-8')


from pymongo import MongoClient
import re

id_dbase = 'live'
client = MongoClient('localhost', 27017)
dbase = client.enable

class User :
    def __init__(self,name,uid) :
        self.name = name
        self.uid = uid
        self.gender = ""
        self.location = []
        self.education = ""

    def printUser(self) :
        print "User Name : ",self.name
        print "UserId : ",self.uid

class Post:
    def __init__(self, url):
        print url
        self.url = url
        self.date = ""
        self.category = ""
        self.plusOned=0
        self.numberOfComments=0
        self.user=''
        self.userId=''
        self.userURL=''
        self.content=[]
        #print 'Done!!!'


    def printPost(self) :
        print 'URL : ',self.url
        print 'Date : ',self.date
        print 'Category : ',self.category
        print '#PlusOned : ',self.plusOned
        print '#Comments : ',self.numberOfComments
        print 'User : ',self.user
        print 'UserId : ',self.userId
        print 'UserURL : ',self.userURL
        print 'Content : ',self.content,

class Comment:
    def __init__(self, url,comment):
        self.postUrl = url
        self.user=''
        self.userId=''
        self.date=''
        self.comment=comment


    def printComment(self) :
        print 'Post URL for this comment: ',self.postUrl
        print 'User : ',self.user
        print 'UserId : ',self.userId
        print 'Comment : ',self.comment

def getPostDate() :
    date_contents = B.find_by_css('.qXj2He')

    #print date_contents.text, '***'
    return date_contents.text

def getCategory() :
    all_categories = B.find_by_css('.UTObDb')
    if len(all_categories) > 0 :
	#print all_categories[0].text, '@@@'
        return all_categories[0].text
    else :
        return "Category Removed/Not Present"

def getPlusOned() :
    plusOned = B.find_by_css('.YxJESd')
    #print (plusOned)[0].text, '!!!'
    if (plusOned)[0].text == '':
        return 0
    else:
        return int((plusOned)[0].text)

def getUser() :
    authorData = B.find_by_css('.m3JvWd')
    authorName = authorData[0].text
    #print authorName, '$$$'
    authorId = B.find_by_css('.xHn24c').find_by_tag('a')['data-profileid']
    #print authorId
    #authorId = re.sub('[!@#$./]', '', authorId)

    return [authorId,authorName]

def getPostContent() :
    try:
        if B.find_by_css('.SlwI7e'):
            all_contents = B.find_by_css('.SlwI7e')
        #print "content"
        #print all_contents[0].text
        #print len(all_contents[0].text)
        #print len(all_contents)
            #print all_content[0].text, '###'
            if len(all_contents[0].text) < 2 :
        #print"*********This is a link text************"
                return all_contents[1].text
        #input()
            if B.find_by_css('.SlwI7e').text:
                return B.find_by_css('.SlwI7e').text #-1 for the pos itself
        else:
            return ""
    except:
        pass
def insertUserInDb(dbase,uname,uid,id_dbase):
    if id_dbase == 'test' :
        userDoc = dbase.test_users
    else :
        userDoc = dbase.users
    user = userDoc.find_one({"name" : uname})
    if user is None : #User does not exist in db
        newUser = User(uname,uid) #Create new user object
        #obj = json.dumps(newUser, default=lambda o: o.__dict__)
        #print obj
        userDoc.insert_one(newUser.__dict__)
        print 'User entered in db : ',uname

def getNumberOfComments() :
    items_ul = B.find_by_css('.AarIR')
    items = items_ul.find_by_tag('li')
    #print len(items), '&&&'
    return len(items) #-1 for the pos itself

def loadPostsInDatabase(dbase,post,id_dbase):
    if id_dbase == 'test' :
        postsDoc = dbase.test_Post
    else :
        postsDoc = dbase.Post
    dpost = postsDoc.find_one({"url" : post.url}) # check if post already exists
    #obj = json.dumps(post, default=lambda o: o.__dict__)
    if dpost is None : #If post url does not exist
        #post.printPost()
        postsDoc.insert_one(post.__dict__)
        print "New post added to db : ",post.url

def loadCommentsInDatabase(dbase,comm,id_dbase):
    if id_dbase == 'test' :
        commentsDoc = dbase.test_Comment
    else :
        commentsDoc = dbase.Comment
    dcomment = commentsDoc.find_one({"comment" : comm.comment})
    #obj = json.dumps(post, default=lambda o: o.__dict__)
    if dcomment is None : #If post url does not exist
        #comm.printComment()
        commentsDoc.insert_one(comm.__dict__)
        print 'Inserted Comment '

def getPostComments() :

	items_ul = B.find_by_css('.AarIR')
	items = items_ul.find_by_tag('li')
	#print items.text
	comment_contents = []
	for item in items:
	    comment_contents.append(item.find_by_css('.g6UaYd').text)
	    #print item.find_by_css('.g6UaYd').text, '%%%'
	return comment_contents
	'''
	if FIND('div.RF5Zd'):
	    all_contents = FIND('ul')[2]
	    if len(all_contents.find_by_tag('li')) < 2 :
	#print"*********This is a link text************"
		return all_contents[1:]
	    return all_contents
	else:
	    while(FIND('div.LCX1Id').text.find('View')==0):
		FIND('span.jBMwef').click()
	    all_contents = FIND('ul')[2]
	    if len(all_contents.find_by_tag('li')) < 2 :
	#print"*********This is a link text************"
		return all_contents[1:]
	    return all_contents
	'''

def getUserURL() :
    """make a list of userIDs and userNames from a single Post's list of Comments"""
    #authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
    usersData=B.find_by_tag('ul')[2] #find the UL that contains the comments
    users=[]
    for data in usersData.find_by_tag('li'):
        userId = data.find_by_tag('a')[1]['data-profileid']
        userId = re.sub('[!@#$./]', '', userId)
        #usern = data.text
        #usern = usern[2:-5]
        userName = data.find_by_tag('a')[1].text
        #print data.text
        users.append([userId,userName])
    return users

def getUserDates():
    y = B.find_by_css('.gmFStc')
    #print [x.text for x in y]
    return [x.text for x in y]


# In[40]:
import dateparser
def addToDB(url):
    print url
    GO(url)
    post = Post(url)
    post.date = dateparser.parse(getPostDate()).isoformat()
    post.category = getCategory()
    post.plusOned = getPlusOned()
    post.numberOfComments = getNumberOfComments()
    userData = getUser()
    post.user = (userData[1])
    post.userId = (userData[0])
    post.userURL = 'https://plus.google.com/' + post.userId + '/about/'
    post.content = getPostContent()

    #all_contents = FIND('ul')[2]
    i=0
    comments =[]
    all_contents = getPostComments()
    allusers = getUserURL()
    alldates = getUserDates()
    allDatesISO = []
    for date in alldates:
        allDatesISO.append(dateparser.parse(date).isoformat())
    try:
        for content in all_contents:

        #print '..',contents.text,'..'
            newComment = Comment(post.url, content)
            newComment.user = allusers[i][1]
            newComment.userId = allusers[i][0]
            newComment.date = allDatesISO[i]
            comments.append(newComment)
            i += 1
    except:
        pass

    #If user not in db :
    insertUserInDb(dbase,post.user,post.userId,id_dbase)
    loadPostsInDatabase(dbase,post,id_dbase)
    #Insert comments only after inserting users#Implement post comment check logic
    for comment in comments :
        insertUserInDb(dbase,comment.user,comment.userId,id_dbase)
        loadCommentsInDatabase(dbase,comment,id_dbase)

print "DONEEEE"


from time import sleep
postList = []
def URLsonPage():
    #postList = set()
    '''while(True):
        B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
        if(FIND('div.EIkL5b').text.find("Looks")==0):
            break
    '''
    #urlRoots=[]
    #urlLinks=FIND('div.DsIcbd')
    len = 300
    count = 0

    while(True):
        urlLinks=B.find_by_css('.DsIcbd')
        #print 'got main URL'
        tempLen = len
        for url in urlLinks:
            try:
                urlRoot = url.find_by_css('a.QXSTae')['href']
                if(urlRoot in postList):
                    continue
                else:
                    count += 1
                    postList.append(urlRoot)
                    print urlRoot
                    #addToDB(urlRoot)
                    #GO('https://plus.google.com/u/0/communities/102497715636887179986')
                    #sleep(1)
                    print count
                    break
            except Exception as e:
                print 'Exception:', e
                continue
        B.execute_script("window.scrollBy(0,500)") #(More...) at bottom


        if B.find_by_css('.EIkL5b').text.find("Looks")==0:
            print B.find_by_css('.EIkL5b').text
            break

    for url in postList:
        try:
            addToDB(url)
        except Exception as e:
            print 'Exception:', e
        sleep(2)


# In[ ]:

URLsonPage()

