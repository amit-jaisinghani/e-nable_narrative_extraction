import csv


def read(csvfile, lines):
    with open(csvfile) as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
                lines.append(row)


def read_file():
    lines_akshai = []
    read('Google_Posts_with__frames_marked.csv', lines_akshai)


    lines_jon = []
    read('jon_labels.csv', lines_jon)

    lines_homan = []
    read('homan_labels.csv', lines_homan)

    return lines_akshai, lines_jon, lines_homan


def get_agreement():

    lines_akshai, lines_jon, lines_homan = read_file()
    #print len(lines_akshai)
    frames_yes_yes_yes = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_yes_yes_no = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_yes_no_yes = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_yes_no_no = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_no_yes_yes = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_no_yes_no = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_no_no_yes = [0, 0, 0, 0, 0, 0, 0, 0]
    frames_no_no_no = [0, 0, 0, 0, 0, 0, 0, 0]

    for i in range(0, 8):
        for j in range(1, 201):
            if lines_akshai[j][i + 2] == '1':
                if lines_homan[j][i + 2] == '1' and lines_jon[j][i + 2] == '1':
                    frames_yes_yes_yes[i] += 1
                elif lines_homan[j][i + 2] == '1':
                    frames_yes_yes_no[i] += 1
                elif lines_jon[j][i + 2] == '1':
                    frames_yes_no_yes[i] += 1
                else:
                    frames_yes_no_no[i] += 1
            else:
                if lines_homan[j][i + 2] == '1' and lines_jon[j][i + 2] == '1':
                    frames_no_yes_yes[i] += 1
                elif lines_homan[j][i + 2] == '1':
                    frames_no_yes_no[i] += 1
                elif lines_jon[j][i + 2] == '1':
                    frames_no_no_yes[i] += 1
                else:
                    frames_no_no_no[i] += 1

    frames = ['Report', 'Device', 'Delivery', 'Progress',
              'Becoming_member', 'Attempt_action', 'Activity', 'Other']

    for i in range(0, 8):
        print '---------------------' \
              '---------------------' \
              '---------------------' \
              '---------------------'
        print frames[i]
        print frames_yes_yes_yes[i], frames_yes_yes_no[i], frames_yes_no_yes[i], frames_yes_no_no[i]
        print frames_no_yes_yes[i], frames_no_yes_no[i], frames_no_no_yes[i], frames_no_no_no[i]
        print frames_yes_yes_yes[i] + frames_no_no_no[i]


    # print frames_yes_yes_yes
    # print frames_yes_yes_no
    # print frames_yes_no_yes
    # print frames_yes_no_no
    # print frames_no_yes_yes
    # print frames_no_yes_no
    # print frames_no_no_yes
    # print frames_no_no_no


get_agreement()
