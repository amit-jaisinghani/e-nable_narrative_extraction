from pymongo import MongoClient
import numpy as np
from matplotlib import pyplot as matplot
from collections import OrderedDict
import plot_old_data

client = MongoClient('localhost:27017')

db = client.faceBook_DB


def read():
    try:
        list_of_dates = []
        posts = db.faceBook_posts_data.find()
        print '\nAll data from Enable FaceBook Database \n'
        for post in posts:
            if post['post_date'] is not '':
                list_of_dates.append(post['post_date'])

    except Exception, e:
        print str(e)

    #print list_of_dates
    print len(list_of_dates)

    count_2013 = 0
    count_2014 = 0
    count_2015 = 0
    count_2016 = 0

    dict_of_months = OrderedDict({'-01-':0, '-02-':1, '-03-':2, '-04-':3, '-05-':4,
                                  '-06-':5, '-07-':6, '-08-':7, '-09-':8, '-10-':9, '-11-':10, '-12-':11})
    count_of_years = {} #[[0 for _ in range(len(list_of_months.keys()))] for _ in range(4)]

    for i in range(5):
        count_of_years[2013 + i] = [0 for j in range(12)]

    for date in list_of_dates:
        for month, ind in dict_of_months.iteritems():
            if month.lower() in str(date).lower():
                if '2013' in str(date):
                    # count_2013 += 1
                    count_of_years[2013][ind] += 1
                if '2014' in str(date):
                    # count_2014 += 1
                    count_of_years[2014][ind] += 1
                if '2015' in str(date):
                    # count_2015 += 1
                    count_of_years[2015][ind] += 1
                if '2016' in str(date):
                    # count_2016 += 1
                    count_of_years[2016][ind] += 1
                if '2017' in str(date):
                    # count_2016 += 1
                    count_of_years[2017][ind] += 1

    for year, month_list in count_of_years.iteritems():
        print('count for', year)
        for month in dict_of_months:
            print(month + ':', month_list[dict_of_months[month]])

    x_list = [2013 + x for x in range(5)]
    y_list = [sum(count_of_years[x_list[i]]) for i in range(len(x_list))]

    for i in range(5):
        print 'count for', x_list[i], ': ', y_list[i]


    x_list_month = []
    y_list_month = []

    for x in x_list:
        i = 0
        x = float(x)
        # for i in range(12):
        while i < 12:
            x_list_month.append(x + float(i) / 12)
            y_list_month.append(count_of_years[x][i])
            i += 1
    return x_list, x_list_month, y_list_month


def plot_graph(x, x1, y1, x2, y2):
    matplot.figure("Monthly distribution of posts", (30, 10))
    matplot.title("Monthly distribution of posts", fontsize=24)
    matplot.plot(x1, y1, 'bs-', label='Facebook posts')
    matplot.plot(x1, y2, 'rs-', label='Google+ posts')
    matplot.plot(x1, y1, 'rs-', label='Sum of Google+ and Facebook posts')
    matplot.plot(x1, [(y1[i] + y2[i]) for i in range(len(y1))], 'ks-',
                 label='Sum of Facebook and Google+ posts')
    matplot.xticks(x, fontsize=20)
    matplot.xlabel('Year', fontsize=20)
    matplot.ylabel('Number of posts', fontsize=20)
    matplot.legend(['Facebook posts', 'Google+ posts', 'Sum of Facebook and Google+ posts'])#, 'upper right')
    matplot.savefig('Monthly distribution of posts.pdf')


x, x1, y1 = read()
x, x2, y2 = plot_old_data.read()
plot_graph(x, x1, y1, x2, y2)