from pymongo import MongoClient
import numpy as np
from matplotlib import pyplot as matplot
from collections import OrderedDict

client = MongoClient('localhost:27017')

db = client.enable


def read():
    try:
        list_of_dates = []
        posts = db.Post.find()
        print '\nAll data from Enable Old Backup Database \n'
        for post in posts:
            if post['date'] is not '':
                list_of_dates.append(post['date'])

    except Exception, e:
        print str(e)

    #print list_of_dates
    print len(list_of_dates)

    count_2013 = 0
    count_2014 = 0
    count_2015 = 0
    count_2016 = 0

    dict_of_months = OrderedDict({'-01-': 0, '-02-': 1, '-03-': 2, '-04-': 3, '-05-': 4,
                                  '-06-': 5, '-07-': 6, '-08-': 7, '-09-': 8, '-10-': 9,
                                  '-11-': 10, '-12-': 11})
    count_of_years = {} #[[0 for _ in range(len(list_of_months.keys()))] for _ in range(4)]

    for i in range(5):
        count_of_years[2013 + i] = [0 for j in range(12)]

    for date in list_of_dates:
        for month, ind in dict_of_months.iteritems():
            if month.lower() in str(date).lower():
                if '2013' in str(date):
                    # count_2013 += 1
                    count_of_years[2013][ind] += 1
                if '2014' in str(date):
                    # count_2014 += 1
                    count_of_years[2014][ind] += 1
                if '2015' in str(date):
                    # count_2015 += 1
                    count_of_years[2015][ind] += 1
                if '2016' in str(date):
                    # count_2016 += 1
                    count_of_years[2016][ind] += 1
                if '2017' in str(date):
                    # count_2016 += 1
                    count_of_years[2017][ind] += 1

    for year, month_list in count_of_years.iteritems():
        print('count for', year)
        for month in dict_of_months:
            print(month + ':', month_list[dict_of_months[month]])

    x_list = [2013 + x for x in range(5)]
    y_list = [sum(count_of_years[x_list[i]]) for i in range(len(x_list))]

    for i in range(5):
        print 'count for', x_list[i], ': ', y_list[i]

    count_total = sum(count_of_years[2013]) \
                  + sum(count_of_years[2014]) + sum(count_of_years[2015]) \
                  + sum(count_of_years[2016]) + sum(count_of_years[2017])
    print count_total


    x_list_month = []
    y_list_month = []

    for x in x_list:
        i = 0
        x = float(x)
        # for i in range(12):
        while i < 12:
            x_list_month.append(x + float(i) / 12)
            y_list_month.append(count_of_years[x][i])
            i += 1

    # matplot.figure("Monthly distribution of Google+ posts", (30, 10))
    # matplot.title("Monthly distribution of Google+ posts", fontsize=24)
    # matplot.plot(x_list_month, y_list_month, 'ks-')
    # matplot.xticks(x_list, fontsize=16)
    # matplot.xlabel('Year', fontsize=22)
    # matplot.ylabel('Number of posts', fontsize=20)
    # matplot.show()
    return x_list, x_list_month, y_list_month


read()