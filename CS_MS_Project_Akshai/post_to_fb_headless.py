from time import sleep
from splinter import Browser
from pymongo import MongoClient
from selenium import webdriver
import sys


def startBrowser(headless=False):
    # TODO don't make B and GO global.  instead, return B use .visit
    global B, GO
    if headless:
        print 'starting headless browser...'
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('window-size=1080,1080')
        B = Browser('chrome', headless=True, options=chrome_options)


    #B = Browser('chrome', headless=headless)  # firefox by default, which gives blank screen


def GO(url):
    print 'Going to', url
    B.visit(url)


def login():
    print('logging in...')
    # B.driver.set_window_size(1080, 1080)
    GO('http://facebook.com')
    sleep(1)
    B.find_by_id('email').fill('jonschull')
    sleep(1)
    B.find_by_id('pass').fill('7Mca8khD')
    sleep(1)
    Btn = B.find_by_id('loginbutton')
    sleep(1)
    Btn.click()
    sleep(1)


def postToWall(post):
    sleep(3)
    B.execute_script("window.scrollBy(0,-1000000000)")  # make sure the Write Something Field is in view
    sleep(2)
    d = B.find_by_css('._1hib').click()  # the 'Write something...' field
    print 'clicked post input'
    nt = B.find_by_css('.notranslate')
    nt.fill(' ' + post['post_content'] + '\n\n' + post['post_link'] + ' ')
    sleep(5)
    nt.clear()
    B.execute_script("window.scrollBy(0, 800)")
    publish = B.find_by_css('._1mf7')
    sleep(3)
    publish.click()
    sleep(4)


def getAllPostsForToday():
    startBrowser(True)
    login()
    GO('https://www.facebook.com/enable.all/')
    sleep(2)
    mongo_client = MongoClient()
    db1 = mongo_client.faceBook_DB
    faceBookPosts = db1.faceBook_posts_data
    db2 = mongo_client.faceBook_DB_stored
    faceBookPostsStored = db2.faceBook_posts_data
    posts = faceBookPosts.find()
    for post in posts:
        try:
            if sys.argv[1] in str(post['post_date']):
                this_post = faceBookPostsStored.find_one({'post_id': post['post_id']})
                if this_post is None and post['post_link'] != '':
                    print post['post_content'], '----'
                    print post['post_link'], '----'
                    postToWall(post)
                    print post
                    faceBookPostsStored.insert_one(post)
        except Exception as e:
            print 'Error:', e
            break
    print 'Quitting browser...'
    B.quit()

    # postToWall()


getAllPostsForToday()
