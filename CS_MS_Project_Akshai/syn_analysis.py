from pymongo import MongoClient
from matplotlib import pyplot as plt
import numpy as np
import nltk
from nltk.corpus import wordnet


def get_db_connection():
    client = MongoClient('localhost:27017')

    db = client.enable

    posts = db.Post.find()
    print 'Total posts:', posts.count()
    return posts, db


def decodeToUTFEight(content):
    try:
        return content.encode('utf-8').decode()
    except UnicodeDecodeError:
        return content.encode('utf-8')


def result_for_frame(posts, keywords):
    result_posts = []
    result_tokens = []
    for post in posts:
        try:
            # print posts['post_content']
            for keyword in keywords:
                if keyword.lower() in post['content'].lower() \
                        and post not in result_posts:
                    # print post['post_content']
                    # print decoded_post_content
                    result_tokens.append(keyword.lower())
                    result_posts.append(post)
        except Exception as e:
            #print e
            continue

    return result_posts, set(result_tokens)


def get_all_synonyms(tokens):

    use_tokens = []
    for word in tokens:
        for syn in wordnet.synsets(word):
            for lemma in syn.lemmas():
                use_tokens.append(lemma.name())

    return use_tokens


def report_frame_analysis():

    report_keywords = ['report', 'summarize', 'summary', 'article'
                       'sum up', 'outline', 'boiled down', 'draft'
                       'synopsis', 'brief', 'describe',
                       'detail', 'communicate', 'result',
                       'designed', 'discuss', 'submit', 'aim',
                       'objective', 'demo', 'document', 'describe'
                       'review', 'release', 'overview', 'efficiency'
                       'benefits', 'conclusion', 'proved', 'final']

    use_keywords = get_all_synonyms(report_keywords)

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    result_posts, result_tokens = result_for_frame(posts, use_keywords)

    print 'Report frame:', len(result_posts)
    print result_tokens
    return len(result_posts)


def device_frame_analysis():

    device_keywords = ['device', 'arm', 'hand', 'limb', 'gadget', 'thumb', 'wrist',
                              'prosthetic', 'prosthesis', 'prostheses',
                              'unlimbited', 'cyborg', 'raptor', 'limbforge',
                              'design', 'beast', 'k1', 'r1', 'oogoo']

    use_keywords = get_all_synonyms(device_keywords)

    posts, db = get_db_connection()


    #print posts.count()
    #print len(posts['post_content'])

    device_posts, device_tokens = result_for_frame(posts, use_keywords)

    print 'Device frame:', len(device_posts)
    print device_tokens

    return len(device_posts)


def delivery_frame_analysis():

    delivery_result_frame = ['delivery', 'deliverer', 'send', 'sent', 'conveyance',
                             'receive', 'transport', 'consignment', 'shipment', 'shipped',
                             'distributed', 'sent', 'got', 'gets', 'gave', 'is here',
                             'loves', 'loved', 'very happy', 'is happy', 'provide',
                             'mail', 'showing off', 'tryout']

    use_keywords = get_all_synonyms(delivery_result_frame)

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    delivery_posts, delivery_tokens = result_for_frame(posts, use_keywords)

    print 'Delivery frame:', len(delivery_posts)
    print delivery_tokens
    return len(delivery_posts)


def progress_frame_analysis():

    progress_result_frame = ['advance', 'burgeon', 'develop', 'maturation', 'making',
                             'progress', 'stagnate', 'improve', 'on track', 'added',
                             'printed', 'printing', 'working', 'build', 'built', 'update'
                             'ready', 'complete', 'prepare', 'preparing', 'checkout',
                             'version', 'process', 'developing', 'change', 'removed', 'running',
                             'creating', 'fixed', 'produce', 'finish', 'derived', 'training',
                             'test', 'experiment', 'experimenting', 'so far', 'investigate',
                             'made', 'almost done', 'inspection', 'plan', 'restore',
                             'about to', 'modified', 'modify', 'result', 'investigate',
                             'assemble', 'increase', 'yet', 'broke', 'fail', 'success',
                             'stage', 'iteration', 'step further', 'vision', 'starting']

    use_keywords = get_all_synonyms(progress_result_frame)

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    progress_posts, progress_tokens = result_for_frame(posts, use_keywords)

    print 'Progress frame:', len(progress_posts)
    print progress_tokens
    return len(progress_posts)


def becoming_member_frame_analysis():

    becoming_member_result_frame = ['enlist', 'enroll', 'enter', 'join',
                                    'sign up', 'register', 'volunteer',
                                    'get started', 'hi ', 'hi,', 'hi!',
                                    'hello everyone', 'hello', 'hey',
                                    'fabricator', 'introduce',  'to help',
                                    'newcomer', 'measured', 'support',
                                    'get involved', 'getting involved',
                                    'my name', 'newbie', 'fabricator',
                                    'start contributing', 'new member',
                                    'new here', 'adding me', 'accepting']

    use_keywords = get_all_synonyms(becoming_member_result_frame)

    posts, db = get_db_connection()

    #print posts.count()
    #print len(posts['post_content'])

    becoming_member_posts, becoming_member_token = result_for_frame(posts, use_keywords)

    print 'Becoming member frame:', len(becoming_member_posts)
    print becoming_member_token
    return len(becoming_member_posts)


def attempt_action_frame_analysis():

    attempt_action_result_frame = ['attempt', 'effort', 'endeavor', 'try',
                                   'undertake', 'go', 'push', 'need', 'trying', 'look at',
                                   'looking for', 'looking to', 'born with', 'lost a', 'like to',
                                   'request', 'tried', 'call for', 'calling out', 'love to']

    posts, db = get_db_connection()

    use_keywords = get_all_synonyms(attempt_action_result_frame)


    #print posts.count()
    #print len(posts['post_content'])

    attempt_action_posts, attempt_tokens = result_for_frame(posts, use_keywords)

    print 'Attempt action frame: ', len(attempt_action_posts)
    print attempt_tokens
    return len(attempt_action_posts)


def activity_frame_analysis():

    activity_result_frame = ['begin', 'commence', 'initiate', 'get started',
                             'open', 'start', 'abandon', 'quit', 'halt',
                             'terminate', 'stop', 'discontinue', 'done', 'conference',
                             'finished', 'through', 'complete', 'conclude', 'event',
                             'wrap up', 'planning', 'meeting', 'workshop', 'seminar',
                             'visited', 'activity', 'exhibit', 'news', 'media', 'contest']

    posts, db = get_db_connection()

    use_keywords = get_all_synonyms(activity_result_frame)

    #print posts.count()
    #print len(posts['post_content'])

    activity_posts, activity_tokens = result_for_frame(posts, use_keywords)

    print 'Activity frame:', len(activity_posts)
    print activity_tokens
    return len(activity_posts)


report_frame_analysis()
device_frame_analysis()
delivery_frame_analysis()
progress_frame_analysis()
becoming_member_frame_analysis()
attempt_action_frame_analysis()
activity_frame_analysis()
