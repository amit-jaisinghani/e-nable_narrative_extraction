import csv
from matplotlib import pyplot as matplot


report_tokens = ['show', 'ending', 'paper', 'identify', 'compact', 'calculate', 'terminal',
                 'theme', 'going', 'get', 'propose', 'rise', 'test', 'outlet', 'report',
                 'loss', 'name', 'button', 'solution', 'leave', 'item', 'release', 'heading',
                 'sketch', 'direct', 'design', 'result', 'pass', 'close', 'event', 'subject',
                 'aim', 'lead', 'sum', 'brief', 'publish', 'state', 'transmit', 'exit', 'succinct',
                 'net', 'issue', 'solvent', 'resume', 'loose', 'communicate', 'free', 'key',
                 'distinguish', 'raise', 'last', 'study', 'bow', 'precis', 'turn', 'place',
                 'consequence', 'documentary', 'purpose', 'vent', 'render', 'point', 'overview',
                 'establish', 'story', 'discus', 'prove', 'submit', 'draft', 'scheme', 'final',
                 'schema', 'finis', 'train', 'particular', 'line', 'present', 'account', 'target',
                 'project', 'posit', 'outcome', 'exhibit', 'bearing', 'demo', 'abstract', 'describe',
                 'evidence', 'demonstration', 'depict', 'end', 'detail', 'take', 'answer', 'document',
                 'exhaust', 'draw', 'trace', 'object', 'effect', 'intent', 'plan', 'summarize', 'outline',
                 'eject', 'try', 'cover', 'drive', 'termination', 'intentional', 'decision']

device_tokens = ['flick', 'deal', 'give', 'twist', 'invention', 'design', 'manus', 'pass',
                 'limb', 'arm', 'gizmo', 'aim', 'thumb', 'script', 'pattern', 'finger',
                 'branch', 'innovation', 'figure', 'cyborg', 'blueprint', 'widget', 'gadget',
                 'mitt', 'reach', 'prosthetic', 'hand', 'beast', 'prosthesis', 'wrist', 'purpose',
                 'device', 'raptor', 'subdivision', 'sleeve', 'weapon', 'paw', 'flip', 'project',
                 'riff', 'intent', 'plan']

delivery_tokens = ['feed', 'founder', 'rapture', 'distribute', 'obtain', 'bring', 'pitch',
                   'go', 'love', 'suffer', 'find', 'mother', 'fix', 'father', 'vex',
                   'charge', 'spread', 'mail', 'sent', 'conveying', 'return', 'rescue',
                   'get', 'beat', 'break', 'beam', 'embark', 'know', 'bringing', 'encounter',
                   'gift', 'draw', 'leave', 'channel', 'become', 'enjoy', 'deal', 'direct',
                   'pass', 'dear', 'transport', 'christ', 'transmit', 'bang', 'impart', 'cause',
                   'pose', 'ply', 'let', 'post', 'come', 'generate', 'receive', 'disseminate',
                   'experience', 'trial', 'place', 'commit', 'load', 'saving', 'render', 'supply',
                   'dispatch', 'jesus', 'send', 'carry', 'apply', 'open', 'transfer', 'throw',
                   'start', 'station', 'lot', 'invite', 'offer', 'afford', 'acquire', 'broadcast',
                   'stick', 'catch', 'hold', 'commence', 'bed', 'air', 'aim', 'meet', 'arrive',
                   'give', 'contribute', 'contract', 'have', 'ship', 'screw', 'develop', 'grant',
                   'provide', 'pay', 'make', 'speech', 'take', 'test', 'eff', 'begin', 'capture',
                   'welcome', 'reach', 'hand', 'produce', 'grow', 'delivery', 'drive', 'allow',
                   'tryout']

progress_tokens = ['bomb', 'flesh', 'over', 'move', 'ending', 'bring', 'shape', 'go',
                   'still', 'perfect', 'upgrade', 'wee', 'promote', 'supply', 'improvement',
                   'better', 'add', 'program', 'mend', 'alter', 'take', 'do', 'hit', 'get',
                   'rise', 'school', 'break', 'aim', 'progress', 'cook', 'betterment', 'realize',
                   'reveal', 'earn', 'name', 'success', 'ended', 'boost', 'hunt', 'solution', 'try',
                   'race', 'realise', 'burgeon', 'die', 'vision', 'set', 'lam', 'soma', 'frame', 'begin',
                   'draw', 'edition', 'design', 'result', 'pass', 'fail', 'further', 'operation', 'event',
                   'append', 'linear', 'leave', 'sue', 'sum', 'lean', 'publish', 'tend', 'encourage', 'version',
                   'print', 'pee', 'reading', 'cause', 'run', 'exam', 'extend', 'leg', 'modify', 'experiment',
                   'ply', 'making', 'assemble', 'come', 'throw', 'change', 'improve', 'advance', 'convert', 'last',
                   'turn', 'range', 'qualification', 'act', 'bod', 'fix', 'feed', 'secure', 'campaign',
                   'point', 'win', 'pop', 'rendering', 'tot', 'determine', 'total', 'establish', 'crap',
                   'raise', 'prepare', 'prove', 'create', 'construct', 'bump', 'approach', 'laze', 'start',
                   'function', 'complete', 'form', 'acquire', 'impress', 'direct', 'train', 'rigid', 'gain',
                   'remote', 'hold', 'commence', 'present', 'made', 'frozen', 'work', 'project', 'sour', 'posit',
                   'meet', 'guide', 'lick', 'have', 'figure', 'give', 'process', 'contribute', 'beforehand',
                   'increment', 'close', 'ready', 'march', 'education', 'mold', 'check', 'develop', 'built',
                   'variety', 'make', 'accomplished', 'scat', 'build', 'test', 'solve', 'educe', 'mould',
                   'evolve', 'play', 'finish', 'lend', 'track', 'reach', 'educate', 'variation', 'forge', 'produce',
                   'plan', 'end', 'grow', 'off', 'clear', 'gather', 'flow', 'part', 'trial', 'tryout', 'review']

becoming_member_tokens = ['documentation', 'innovate', 'have', 'figure', 'show', 'metric', 'participate',
                          'back', 'accept', 'assess', 'subscribe', 'connect', 'file', 'measure', 'howdy',
                          'keep', 'digest', 'living', 'funding', 'voluntary', 'confirm', 'union', 'sum',
                          'offer', 'brook', 'unite', 'fabricator', 'draft', 'take', 'sustain', 'abide',
                          'junction', 'articulation', 'read', 'introduce', 'embark', 'hi', 'link', 'bear',
                          'precede', 'hold', 'present', 'volunteer', 'insert', 'enroll', 'engage', 'join',
                          'metrical', 'enlist', 'register', 'recruit', 'value', 'hello', 'record', 'starter',
                          'stand', 'enter', 'newcomer', 'support', 'registry']

attempt_action_tokens = ['essay', 'exertion', 'motivation', 'depart', 'undertake', 'campaign',
                         'judge', 'tackle', 'move', 'tug', 'sample', 'need', 'examine', 'crack',
                         'want', 'pass', 'go', 'endeavour', 'endeavor', 'petition', 'seek',
                         'guarantee', 'crowd', 'lead', 'prove', 'feat', 'travel', 'offer', 'thrust',
                         'attack', 'labor', 'start', 'live', 'sweat', 'xtc', 'take', 'rifle',
                         'test', 'strive', 'cause', 'exploit', 'movement', 'function', 'force',
                         'run', 'tried', 'get', 'press', 'spell', 'travail', 'break', 'render',
                         'quest', 'hear', 'become', 'ask', 'last', 'promote', 'effort', 'demand',
                         'proceed', 'sound', 'involve', 'stress', 'attempt', 'fit', 'strain', 'die',
                         'require', 'work', 'request', 'drive', 'contract', 'try', 'turn', 'tour',
                         'adam', 'enterprise', 'push', 'x', 'fail', 'operate', 'blend']

activity_tokens = ['shop', 'show', 'scratch', 'over', 'kickoff', 'converge', 'planning', 'through',
                   'go', 'perfect', 'fit', 'spread', 'match', 'do', 'march', 'halt', 'get', 'showing',
                   'practice', 'stop', 'break', 'game', 'candid', 'provision', 'encounter', 'arrange',
                   'closure', 'bar', 'bulge', 'cause', 'contain', 'set', 'exposed', 'begin', 'competition',
                   'freeze', 'jump', 'touch', 'see', 'design', 'result', 'close', 'event', 'subject',
                   'pioneer', 'issue', 'loose', 'stem', 'reason', 'offset', 'assemble', 'news', 'come',
                   'league', 'receive', 'initiate', 'accomplished', 'act', 'block', 'first', 'depart',
                   'point', 'manage', 'pop', 'open', 'conference', 'quit', 'capable', 'visit', 'start',
                   'workshop', 'call', 'meeting', 'empty', 'medium', 'complete', 'afford', 'serve', 'part',
                   'catch', 'hold', 'commence', 'present', 'case', 'nail', 'word', 'arrant', 'project',
                   'plosive', 'can', 'outdoors', 'meet', 'give', 'demo', 'surface', 'chat', 'dress',
                   'check', 'fill', 'end', 'lame', 'perform', 'make', 'novice', 'fire', 'answer', 'fare',
                   'play', 'finish', 'effect', 'stay', 'plan', 'overt', 'clear', 'gather', 'tyro']


'''
activity_result_frame = ['begin', 'commence', 'initiate', 'get started',
                         'open', 'start', 'abandon', 'quit', 'halt',
                         'terminate', 'stop', 'discontinue', 'done', 'conference',
                         'finished', 'through', 'complete', 'conclude', 'event',
                         'wrap up', 'planning', 'meeting', 'workshop', 'seminar',
                         'visited', 'activity', 'exhibit', 'news', 'media', 'contest']


attempt_action_result_frame = ['attempt', 'effort', 'endeavor', 'try',
                               'undertake', 'go', 'push', 'need', 'trying', 'look at',
                               'looking for', 'looking to', 'born with', 'lost a', 'like to',
                               'request', 'tried', 'call for', 'calling out', 'love to']


progress_result_frame = ['advance', 'burgeon', 'develop', 'maturation', 'making',
                         'progress', 'stagnate', 'improve', 'on track', 'added',
                         'printed', 'printing', 'working', 'build', 'built', 'update',
                         'ready', 'complete', 'prepare', 'preparing', 'checkout',
                         'version', 'process', 'developing', 'change', 'removed', 'running',
                         'creating', 'fixed', 'produce', 'finish', 'derived', 'training',
                         'test', 'experiment', 'experimenting', 'so far', 'investigate',
                         'made', 'almost done', 'inspection', 'plan', 'restore',
                         'about to', 'modified', 'modify', 'result', 'investigate',
                         'assemble', 'increase', 'yet', 'broke', 'fail', 'success',
                         'stage', 'iteration', 'step further', 'vision', 'starting']


becoming_member_result_frame = ['enlist', 'enroll', 'enter', 'join',
                                'sign up', 'register', 'volunteer',
                                'get started', 'hi ', 'hi,', 'hi!',
                                'hello everyone', 'hello', 'hey',
                                'fabricator', 'introduce',  'to help',
                                'newcomer', 'measured', 'support',
                                'get involved', 'getting involved',
                                'my name', 'newbie', 'fabricator',
                                'start contributing', 'new member',
                                'new here', 'adding me', 'accepting']


delivery_report_frame = ['delivery', 'deliverer', 'send', 'sent', 'conveyance',
                         'receive', 'transport', 'consignment', 'shipment', 'shipped',
                         'distributed', 'sent', 'got', 'gets', 'gave', 'is here',
                         'loves', 'loved', 'very happy', 'is happy', 'provide',
                         'mail', 'showing off', 'tryout']


device_report_keywords = ['device', 'arm', 'hand', 'limb', 'gadget', 'thumb', 'wrist',
                          'prosthetic', 'prosthesis', 'prostheses',
                          'unlimbited', 'cyborg', 'raptor', 'limbforge',
                          'design', 'beast', 'k1', 'r1', 'oogoo']


report_keywords = ['report', 'summarize', 'summary', 'article',
                   'sum up', 'outline', 'boiled down', 'draft',
                   'synopsis', 'brief', 'describe',
                   'detail', 'communicate', 'result',
                   'designed', 'discuss', 'submit', 'aim',
                   'objective', 'demo', 'document', 'describe',
                   'review', 'release', 'overview', 'efficiency',
                   'benefits', 'conclusion', 'proved', 'final']

'''


def check_list(data_points, url_list, col_num, store=False):
    tp = 0
    fp = 0
    fn = 0
    tn = 0
    fn_list, fp_list = [], []
    for idx in data_points:
        if idx[0] in url_list:
            if idx[col_num] == '1':
                tp += 1
            else:
                fp += 1
                if store:
                    fp_list.append(idx)
        else:
            if idx[col_num] == '1':
                fn += 1
                if store:
                    fn_list.append(idx)
            else:
                tn += 1
    #print(tp, fp, fn, tn)
    if store:
        print('False Positives:', fp_list)
        print('False Negatives:', fn_list)
    return tp, fp, fn, tn


def get_frame_fit(data_points, tokens, word):
    result = set()
    for data in data_points:
        try:
            # print posts['post_content']
            for frame_token in tokens:
                if frame_token.lower() in data[1].lower():
                    # print post['post_content']
                    # print decoded_post_content
                    result.add(data[0])

            if word.lower() in data[1].lower():
                # print post['post_content']
                # print decoded_post_content
                result.add(data[0])
        except Exception as e:
            print e
            continue
    #print result
    return result


def read_file():
    lines = []
    with open('Google_Posts__with_frames_marked.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            lines.append(row)

    return lines


def plot_graph(x_list, y_list, frame):

    for i in range(len(x_list)):
        matplot.plot((x_list[i]), (y_list[i]), label=frame[i][1], marker=".")

    # matplot.plot(x1, [(y1[i] + y2[i]) for i in range(len(y1))], 'ks-',
    #              label='Sum of Facebook and Google+ posts')

    matplot.ylabel('TPR', fontsize=20)
    matplot.xlabel('FPR', fontsize=20)
    matplot.legend(['Report', 'Device', 'Delivery', 'Progress',
                    'Becoming_member', 'Attempt_action', 'Activity'])#, 'Sum of Facebook and Google+ posts'])#, 'upper right')
    matplot.savefig('ROC_for_posts.pdf')


def main():

    data_points = read_file()[1:]

    frames = [[0, 'Report'], [1, 'Device'],
              [2, 'Delivery'], [3, 'Progress'],
              [4, 'Becoming_member'], [5, 'Attempt_action'],
              [6, 'Activity'], [7, 'Other']]


    tokens = []
    '''
    tokens.append(report_keywords)
    tokens.append(device_report_keywords)
    tokens.append(delivery_report_frame)
    tokens.append(progress_result_frame)
    tokens.append(becoming_member_result_frame)
    tokens.append(attempt_action_result_frame)
    tokens.append(activity_result_frame)'''

    tokens.append(report_tokens)
    tokens.append(device_tokens)
    tokens.append(delivery_tokens)
    tokens.append(progress_tokens)
    tokens.append(becoming_member_tokens)
    tokens.append(attempt_action_tokens)
    tokens.append(activity_tokens)

    tpr_list_list = []
    fpr_list_list = []
    for i in range(0, 7):
        max_precision = 0
        max_recall = 0
        max_f1 = 0
        final_tokens = []
        tpr_list = []
        fpr_list = []
        print 'Frame:', frames[i][1], 'Started!'
        for word in tokens[i]:
            result = get_frame_fit(data_points, final_tokens, word)
            tp, fp, fn, tn = check_list(data_points, result, i + 2)
            recall = 0
            fpr = 0
            #total = (tp + tn + fp + fn)
            if tp + fp != 0:
                precision = (tp / float(tp + fp))
            # total_precision += precision
            if tp + fn != 0:
                recall = (tp / float(tp + fn))
            if fp + tn != 0:
                fpr = (fp / float(fp + tn))
            fpr_list.append(fpr)
            tpr_list.append(recall)
            #print fpr, recall
            final_tokens.append(word)

        print 'Frame:', frames[i][1], 'Done!'

        tpr_list_list.append(tpr_list)
        fpr_list_list.append(fpr_list)

    plot_graph(fpr_list_list, tpr_list_list, frames)


if __name__ == '__main__':
    main()