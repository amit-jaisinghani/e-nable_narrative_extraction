import csv

def file_check(file, permission):
    """
    Creates a file handler.
    :param file: Name of the file
    :param permission: Permission
    :return: File handler
    """
    try:
        f = open(file, permission)
        return f
    except Exception:
        print("File", file, "does not exist...")
        exit()


def read_csv(file):
    """
    Reads the csv file  and converts it into data points.
    :param file: File handler
    :return: list of data points.
    """
    attributes = file.readline()
    data_points = []
    for line in file:
        line = line.strip().split(',')
        data_points.append(line)
    return attributes, data_points


def check_list(data_points, url_list, col_num, store=False):
    tp = 0
    fp = 0
    fn = 0
    tn = 0
    fn_list, fp_list = [], []
    count = 0
    for idx in data_points:
        if idx[0] in url_list:
            if idx[col_num] == '1':
                tp += 1
            else:
                fp += 1
                if store:
                    count += 1
                    fp_list.append(idx[0])
        else:
            if idx[col_num] == '1':
                fn += 1
                if store:
                    fn_list.append(idx[0])
            else:
                tn += 1
    #print(tp, fp, fn, tn)
    if store:
        print('False Positives:', fp_list)
        print('False Negatives:', fn_list)
    return tp, fp, fn, tn


def get_frame_fit(data_points, frame_tokens):
    result = []
    for data in data_points:
        try:
            # print posts['post_content']
            for frame_token in frame_tokens:
                if frame_token.lower() in data[1].lower() \
                        and data[0] not in result:
                    # print post['post_content']
                    # print decoded_post_content
                    result.append(data[0])
        except Exception as e:
            print e
            continue

    return result


activity_result_frame = ['begin', 'commence', 'initiate', 'get started',
                         'open', 'start', 'abandon', 'quit', 'halt',
                         'terminate', 'stop', 'discontinue', 'done', 'conference',
                         'finished', 'through', 'complete', 'conclude', 'event',
                         'wrap up', 'planning', 'meeting', 'workshop', 'seminar',
                         'visited', 'activity', 'exhibit', 'news', 'media', 'contest']


attempt_action_result_frame = ['attempt', 'effort', 'endeavor', 'try',
                               'undertake', 'go', 'push', 'need', 'trying', 'look at',
                               'looking for', 'looking to', 'born with', 'lost a', 'like to',
                               'request', 'tried', 'call for', 'calling out', 'love to']


progress_result_frame = ['advance', 'burgeon', 'develop', 'maturation', 'making',
                         'progress', 'stagnate', 'improve', 'on track', 'added',
                         'printed', 'printing', 'working', 'build', 'built', 'update',
                         'ready', 'complete', 'prepare', 'preparing', 'checkout',
                         'version', 'process', 'developing', 'change', 'removed', 'running',
                         'creating', 'fixed', 'produce', 'finish', 'derived', 'training',
                         'test', 'experiment', 'experimenting', 'so far', 'investigate',
                         'made', 'almost done', 'inspection', 'plan', 'restore',
                         'about to', 'modified', 'modify', 'result', 'investigate',
                         'assemble', 'increase', 'yet', 'broke', 'fail', 'success',
                         'stage', 'iteration', 'step further', 'vision', 'starting']

becoming_member_result_frame = ['enlist', 'enroll', 'enter', 'join',
                                'sign up', 'register', 'volunteer',
                                'get started', 'hi ', 'hi,', 'hi!',
                                'hello everyone', 'hello', 'hey',
                                'fabricator', 'introduce',  'to help',
                                'newcomer', 'measured', 'support',
                                'get involved', 'getting involved',
                                'my name', 'newbie', 'fabricator',
                                'start contributing', 'new member',
                                'new here', 'adding me', 'accepting']


delivery_report_frame = ['delivery', 'deliverer', 'send', 'sent', 'conveyance',
                         'receive', 'transport', 'consignment', 'shipment', 'shipped',
                         'distributed', 'sent', 'got', 'gets', 'gave', 'is here',
                         'loves', 'loved', 'very happy', 'is happy', 'provide',
                         'mail', 'showing off', 'tryout']


device_report_keywords = ['device', 'arm', 'hand', 'limb', 'gadget', 'thumb', 'wrist',
                          'prosthetic', 'prosthesis', 'prostheses',
                          'unlimbited', 'cyborg', 'raptor', 'limbforge',
                          'design', 'beast', 'k1', 'r1', 'oogoo']


report_keywords = ['report', 'summarize', 'summary', 'article',
                   'sum up', 'outline', 'boiled down', 'draft',
                   'synopsis', 'brief', 'describe',
                   'detail', 'communicate', 'result',
                   'designed', 'discuss', 'submit', 'aim',
                   'objective', 'demo', 'document', 'describe',
                   'review', 'release', 'overview', 'efficiency',
                   'benefits', 'conclusion', 'proved', 'final']


def read_file():
    lines = []
    with open('Google_Posts__with_frames_marked.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            lines.append(row)

    return lines


def main():

    tokens = []
    tokens.append(report_keywords)
    tokens.append(device_report_keywords)
    tokens.append(delivery_report_frame)
    tokens.append(progress_result_frame)
    tokens.append(becoming_member_result_frame)
    tokens.append(attempt_action_result_frame)
    tokens.append(activity_result_frame)

    data_points = read_file()[1:]
    frames = [[0, 'Report'], [1, 'Device'],
              [2, 'Delivery'], [3, 'Progress'],
              [4, 'Becoming_member'], [5, 'Attempt_action'],
              [6, 'Activity'], [7, 'Other']]

    total_precision = 0
    total_recall = 0
    for i in range(0, 7):
        result = get_frame_fit(data_points, tokens[i])
        tp, fp, fn, tn = check_list(data_points, result, i + 2)
        print '------------------------------------------' \
              '-----------------------------------------'
        print 'Frame:', frames[i][1]
        precision = (tp / float(tp + fp))
        total_precision += precision
        recall = (tp / float(tp + fn))
        total_recall += recall
        print 'Precision', precision * 100
        print 'Recall', recall * 100
        print 'F1', (2 * float((precision * recall) / (precision + recall))) * 100
        print '------------------------------------------' \
              '-----------------------------------------'

    #print total_precision / float(8) * 100
    #print total_recall / float(8) * 100


if __name__ == '__main__':
    main()

