
# coding: utf-8

# In[38]:

import time

from selenium import webdriver
from splinter import Browser

def startBrowser(headless=False):
    #TODO don't make B and GO global.  instead, return B use .visit
    
    if headless:
        print 'starting headless browser...'
    global B, GO 
    B=Browser('chrome',headless=headless) #firefox by default, which gives blank screen
    FIND=B.find_by_css
'''
#executable_path = {'executable_path':'C:\Users\Akshai\Downloads\chromedriver.exe'}

executable_path = {'executable_path':'/usr/bin/chromedriver'}

B = Browser('chrome', **executable_path) #firefox by default
FIND=B.find_by_css
GO=B.visit
'''
def login():
    GO('https://accounts.google.com/ServiceLogin?passive=1209600&osid=1&continue=https://plus.google.com/collections/featured&followup=https://plus.google.com/collections/featured#identifier')
    #GO('https://plus.google.com/u/0/communities/102497715636887179986')
    B.find_by_id('identifierId').fill('akshai.jb007@gmail.com')
    B.find_by_id('identifierNext').click()
    time.sleep(2)
    time.sleep(2)
    B.find_by_id('passwordNext').click()

def init(headless=False):
    startBrowser(headless)
    sleep(4)
    login()
    sleep(4)
    GO('https://plus.google.com/u/0/communities/102497715636887179986')


# In[ ]:

#!conda install selenium


# In[ ]:

#!pip uselenium


# In[138]:
'''
def URLsonPage():
    urlRoots=[]
    urlLinks=FIND('div.DsIcbd')
    for url in urlLinks:
        urlRoot = url.find_by_css('a.QXSTae')['href']
        urlRoots.append(urlRoot)
    return urlRoots
        
#URLsonPage()

def saveURLS():
    urls=URLsonPage()
    filename= 'urls' + str(len(urls)) +'.txt'
    print '==Saving', filename
    file=open(filename,'w')
    
    for url in urls:
        file.write(url+'\n')
    file.close()
    return filename

filename = saveURLS()
#print open(filename,'r').read()


# In[139]:

"""
This a continuous loop.
To terminate it, put a file named "STOP" in the current directory.
"""
import os.path
from time import sleep
while 1:
    if os.path.isfile('STOP'): 
        print 'STOPFILE FOUND.'
        break
    else: 
        B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
        saveURLS()
        sleep(4)
print 'DONE?'
'''

# In[39]:

from pymongo import MongoClient 
import re

id_dbase = 'live'
client = MongoClient('localhost', 27017)
dbase = client.enable

class User :
    def __init__(self,name,uid) :
        self.name = name
        self.uid = uid
        self.gender = ""
        self.location = []
        self.education = ""

    def printUser(self) :
        print "User Name : ",self.name
        print "UserId : ",self.uid

class Post:
    def __init__(self, url):
        print url
        self.url = url
        self.date = ""
        self.category = ""
        self.plusOned=0
        self.numberOfComments=0
        self.user=''
        self.userId=''
        self.userURL=''
        self.content=[]
        #print 'Done!!!'


    def printPost(self) :
        print 'URL : ',self.url
        print 'Date : ',self.date
        print 'Category : ',self.category
        print '#PlusOned : ',self.plusOned
        print '#Comments : ',self.numberOfComments
        print 'User : ',self.user
        print 'UserId : ',self.userId
        print 'UserURL : ',self.userURL
        print 'Content : ',self.content,

class Comment:
    def __init__(self, url,comment):
        self.postUrl = url
        self.user=''
        self.userId=''
        self.date=''
        self.comment=comment


    def printComment(self) :
        print 'Post URL for this comment: ',self.postUrl
        print 'User : ',self.user
        print 'UserId : ',self.userId
        print 'Comment : ',self.comment
        
def getPostDate() :
    date_contents = FIND('span.uv.PL')
    for contents in date_contents :
        date = contents.text
        return date
        
def getCategory() :
    all_categories = FIND('span.Wt.Hm.Ve.tr')
    if len(all_categories) > 0 :
        return all_categories[0].text
    else :
        return "Category Removed/Not Present"

def getPlusOned() :
    plusOned = FIND('div.YxJESd')
    return (plusOned)[0].text

def getUser() :
    authorData = FIND('div.xHn24c')
    authorId = authorData[0].find_by_css('a')['href']
    authorId = re.sub('[!@#$./]', '', authorId)
    authorName = authorData[0].text
    return [authorId,authorName]

def getPostContent() :
    try:
        if FIND('div.wftCae'):
            all_contents = FIND('div.wftCae')
        #print "content"
        #print all_contents[0].text
        #print len(all_contents[0].text)
        #print len(all_contents)
            if len(all_contents[0].text) < 2 :
        #print"*********This is a link text************"
                return all_contents[1].text
        #input()
            if FIND('div.wftCae').text:
                return FIND('div.wftCae').text #-1 for the pos itself
        else:
            return ""
    except:
        pass
def insertUserInDb(dbase,uname,uid,id_dbase):
    if id_dbase == 'test' :
        userDoc = dbase.test_users
    else :
        userDoc = dbase.users
    user = userDoc.find_one({"name" : uname})
    if user is None : #User does not exist in db
        newUser = User(uname,uid) #Create new user object
        #obj = json.dumps(newUser, default=lambda o: o.__dict__)
        #print obj
        userDoc.insert_one(newUser.__dict__)
        print 'User entered in db : ',uname

def getNumberOfComments() :
    return len( FIND('div.wftCae')) - 1 #-1 for the pos itself

def loadPostsInDatabase(dbase,post,id_dbase):
    if id_dbase == 'test' :
        postsDoc = dbase.test_Post
    else :
        postsDoc = dbase.Post
    dpost = postsDoc.find_one({"url" : post.url}) # check if post already exists
    #obj = json.dumps(post, default=lambda o: o.__dict__)
    if dpost is None : #If post url does not exist
        postsDoc.insert_one(post.__dict__)
        print "New post added to db : ",post.url

def loadCommentsInDatabase(dbase,comm,id_dbase):
    if id_dbase == 'test' :
        commentsDoc = dbase.test_Comment
    else :
        commentsDoc = dbase.Comment
    dcomment = commentsDoc.find_one({"comment" : comm.comment})
    #obj = json.dumps(post, default=lambda o: o.__dict__)
    if dcomment is None : #If post url does not exist
        commentsDoc.insert_one(comm.__dict__)
        print 'Inserted Comment '

def getPostComments() :
    try:
        if FIND('div.RF5Zd'):
            all_contents = FIND('ul')[2]
            if len(all_contents.find_by_tag('li')) < 2 :
        #print"*********This is a link text************"
                return all_contents[1:]
            return all_contents
        else:
            while(FIND('div.LCX1Id').text.find('View')==0):
                FIND('span.jBMwef').click()
            all_contents = FIND('ul')[2]
            if len(all_contents.find_by_tag('li')) < 2 :
        #print"*********This is a link text************"
                return all_contents[1:]
            return all_contents
    except Exception:
        pass
    
def getUserURL() :
    """make a list of userIDs and userNames from a single Post's list of Comments"""
    #authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
    usersData=FIND('ul')[2] #find the UL that contains the comments
    users=[]
    for data in usersData.find_by_tag('li'):
        userId = data.find_by_tag('a')[1]['data-profileid']
        userId = re.sub('[!@#$./]', '', userId)
        #usern = data.text
        #usern = usern[2:-5]
        userName = data.find_by_tag('a')[1].text
        #print data.text
        users.append([userId,userName])
    return users

def getUserDates():
    y = FIND("div.gmFStc")
    return [x.text for x in y]


# In[40]:

def addToDB(url):
    print url
    GO(url)
    post = Post(url)
    post.date = getPostDate()
    post.category = getCategory()
    post.plusOned = getPlusOned()
    post.numberOfComments = getNumberOfComments()
    post.user = (getUser()[1])
    post.userId = (getUser()[0])
    post.userURL = 'https://plus.google.com/' + post.userId + '/about/'
    post.content = getPostContent()

    #all_contents = FIND('ul')[2]
    i=0
    comments =[]
    all_contents = getPostComments()
    allusers = getUserURL()
    alldates = getUserDates()
    try: 
        for contents in all_contents.find_by_tag('li') :
            
        #print '..',contents.text,'..'
            newComment = Comment(post.url,contents.text)
            newComment.user = allusers[i-1][1]
            newComment.userId = allusers[i-1][0]
            newComment.date = alldates[i-1] 
            comments.append(newComment)
    except:
        pass
    
    #If user not in db :
    insertUserInDb(dbase,post.user,post.userId,id_dbase)
    loadPostsInDatabase(dbase,post,id_dbase)
    #Insert comments only after inserting users#Implement post comment check logic
    for comment in comments :
        insertUserInDb(dbase,comment.user,comment.userId,id_dbase)
        loadCommentsInDatabase(dbase,comment,id_dbase)

print "DONEEEE" 


# In[ ]:
'''
f = open("except_urls.txt")
for url in f:
        print url
        GO(url)
        post = Post(url)
        post.date = getPostDate()
        post.category = getCategory()
        post.plusOned = getPlusOned()
        post.numberOfComments = getNumberOfComments()
        post.user = (getUser()[1])
        post.userId = (getUser()[0])
        post.userURL = 'https://plus.google.com/' + post.userId + '/about/'
        post.content = getPostContent()

        #all_contents = FIND('ul')[2]
        i=0
        comments =[]
        all_contents = getPostComments()
        allusers = getUserURL()
        alldates = getUserDates()
        for contents in all_contents.find_by_tag('li') :
            
            #print '..',contents.text,'..'
            newComment = Comment(post.url,contents.text)
            newComment.user = allusers[i-1][1]
            newComment.userId = allusers[i-1][0]
            newComment.date = alldates[i-1] 
            comments.append(newComment)
            
        #If user not in db :

        insertUserInDb(dbase,post.user,post.userId,id_dbase)
        loadPostsInDatabase(dbase,post,id_dbase)
        #Insert comments only after inserting users#Implement post comment check logic
        for comment in comments :
            insertUserInDb(dbase,comment.user,comment.userId,id_dbase)
            loadCommentsInDatabase(dbase,comment,id_dbase)

print DONEEEE        
        
        


# In[2]:

from time import sleep
postList = []

def URLsonPage():
    #postList = set()
    while(True):
        B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
        if(FIND('div.EIkL5b').text.find("Looks")==0):
            break
    
    #urlRoots=[]
    #urlLinks=FIND('div.DsIcbd')
    while(True):
        if FIND('div.EIkL5b'):
            if FIND('div.EIkL5b').text.find("Looks")==0:
                break
        urlLinks=FIND('div.DsIcbd')
        for counter in count:
                B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
        if set(urlLinks) & set(postList) == set(urlLinks) or len(set(urlLinks)) == len(set(postList)):
            B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
            count+=1
            
        for url in urlLinks:
            urlRoot = url.find_by_css('a.QXSTae')['href']
            if(urlRoot in postList):
                continue
            else:
                postList.append(urlRoot)
                addToDB(urlRoot)
                GO('https://plus.google.com/u/0/communities/102497715636887179986')
                sleep(2)
                break
        
    
'''

# In[41]:

from time import sleep
postList = []
def URLsonPage():
    #postList = set()
    '''while(True):
        B.execute_script("window.scrollBy(0,20000)") #(More...) at bottom
        if(FIND('div.EIkL5b').text.find("Looks")==0):
            break
    '''
    #urlRoots=[]
    #urlLinks=FIND('div.DsIcbd')
    len = 300
    while(True):
        
        urlLinks=FIND('div.DsIcbd')
        tempLen = len   
        for url in urlLinks:
            urlRoot = url.find_by_css('a.QXSTae')['href']
            if(urlRoot in postList):
                continue
            else:
                postList.append(urlRoot)
                print urlRoot
                #addToDB(urlRoot)
                #GO('https://plus.google.com/u/0/communities/102497715636887179986')
                #sleep(1)
                break
        
        B.execute_script("window.scrollBy(0,100)") #(More...) at bottom
		
        
        if FIND('div.EIkL5b').text.find("Looks")==0:
            print FIND('div.EIkL5b').text
            break
        			
    for url in postList:
        addToDB(url)
        sleep(2)


# In[ ]:

URLsonPage()

'''
# In[36]:

FIND('div.EIkL5b').text.find("Looks")


# In[78]:

print postList


# In[ ]:

import thread
import threading

def raw_input_with_timeout(prompt, timeout=30.0):
    print prompt,    
    timer = threading.Timer(timeout, thread.interrupt_main)
    astring = None
    try:
        timer.start()
        astring = raw_input(prompt)
    except KeyboardInterrupt:
        pass
    timer.cancel()
    return astring

raw_input_with_timeout("abc", 30)

'''
# In[ ]:




# In[ ]:



