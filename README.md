Note: Please don’t modify any files in CS_MS_Project_Akshai folder, 
if you would still like to modify please make a copy for that file and then modify the copy.

- Every developer needs to work on their own respective branch. So please create a new branch off master branch.

- All your code changes and other file changes should first be pushed to your own branch and reviewed by the professor.
	This is done to avoid conflicting and errorneous software becoming a part of other branches.

- Once reviewed, merge your branch with readyToProduction and push all merge changes.

- Digital ocean droplet uses readyToProduction branch. Please don't change the branch that the droplet uses.

- After pushing the merge changes a simple git pull command should update the droplet with your new code.

- Once the code and the files are tested and executes correctly, merge readyToProduction branch into master branch.
	DO NOT merge your branch directly into master branch.
	
- In order to use 


1. To use the repository please install git using instructions provided in this link - https://git-scm.com/book/en/v2/Getting-Started-Installing-Git. 
- Create a bitbucket account.
- Please contact Dr. Christopher Homan (cmh@cs.rit.edu) to be added as a user for the repository.

2. After being added as a user to the repository, to clone the repository 
please use command - ‘https://<username>@bitbucket.org/enableviz/enableviz.git’ 
and when prompted enter your bitbucket account password.
After cloning:
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai 
	- chmod u+x *.sh
	- chmod u+x *.py

	- The last two commands will make the scripts and python file executable by the user

3. - Install anaconda python 2.7 using instructions provided in this link - https://conda.io/docs/user-guide/install/index.html#regular-installation
- install pip using instructions provided in this link - https://pip.pypa.io/en/stable/installing/
- install packages needed to execute the scripts using pip - pip install <package_name>
- some of the packages that are required are selenium, splinter, csv, pandas, datetime, json, pymongo, matplotlib, nltk

4. Install MongoDB to store data using instructions provided in this link - https://docs.mongodb.com/v3.4/installation/
	To export data from MongoDB use command - mongoexport  --db <database> --collection <collection> --out <filename>.json
	To import data into MongoDB use command - mongoimport --db <database> --collection <collection> --file <filename>
		For import it is suggested to use ‘.json’ type file

5. To execute Facebook page scraper:
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai 
	- ./run_fb_page_scraper.sh
		This script gets the previous day's and current day's posts and comments from Facebook.
	- if you would like get the posts and comments for a different time interval, run the following command:
	- python fb_page_scraper.py <start_date> <end_date>
		where <start_date> specify the oldest posts and comments and <end_date> the latest posts and comments to be extracted
		date format for both: YYYY-MM-DD

6. To execute Google+ page scraper
- please install chromedriver using instruction provided in this link - https://gist.github.com/ziadoz/3e8ab7e944d02fe872c3454d17af31a5
- go to repository’s home directory i.e. enableviz and use commands as follows (uses Non-headless browser) :
	- cd CS_MS_Project_Akshai
	- ./run_google_members_scraper.sh (this will take a while to extract all the member names, let it run to completion)
	- ./run_google_plus_scraper.sh
	Note: Use this to get data since e-NABLE Google+ page was created
		Use the steps below to extract just the recent data
- go to repository’s home directory i.e. enableviz and use commands as follows (uses Non-headless browser) :
	- cd CS_MS_Project_Akshai
	- ./google_plus_recent_post_scraper.sh


Note: To successfully run these file the MongoDB in use should have posts from Google+ and Facebook
			so please execute tasks 5 and 6 before executing tasks 7 and 8.
			
7. To obtain the Monthly distribution of posts graph
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai
	- ./run_monthly_distribution_graph_generator.sh
		- This will save a file: Monthly distribution of posts.pdf in CS_MS_Project_Akshai directory

8. To obtain the results from the machine learning program in form of an ROC
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai
	- ./run_roc_generator.sh
		- This will save a file: Monthly distribution of posts.pdf in CS_MS_Project_Akshai directory

9. To obtain the results from the machine learning program validation in the form of precision, recall and F1 scores
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai
	- ./run_validation.sh

10. To obtain the inter-annotator agreement results 
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai
	- ./run_inter_annotator_agreement.sh

11. To post to facebook enable.all wall (This is the script that used to post new updates to enable.all on a daily basis)
- This script uses system time to get yesterday's date and changes the date format as per the program's requirements 
		(first two lines of the script does this).
- Extract data from Facebook (using 5).
- go to repository’s home directory i.e. enableviz and use commands as follows:
	- cd CS_MS_Project_Akshai
	- ./run_headless_facebook_poster.sh
	- This code does not post the same content twice as it stores the contents that are posted in MongoDB (dbname - faceBook_DB_stored). 
		However, this will only work if executed multiple times in the same machine.
	- Please refrain from executing this script from two different machines on the same day. 
	- If you still wish to do so, please delete the duplicate posts from enable.all wall.
	
12. CSV files used for inter-annotator agreement can found in directory - enableviz/CSV_Files_Akshai_Project
- The files have been named according to the annotator who labelled them


How to use git:

Git is a version control system. There three basic steps or rules to follow all the time in this same order
pull, commit and push

The best way to use git is with a software like sourcetree or smartgit (both are equally good, but sourcetree doesn't support linux).
sourcetree download link: https://www.sourcetreeapp.com/
smartgit download link: http://www.syntevo.com/smartgit/

You don't need to use the commands below if you are using sourcetree or smartgit 
	as these softwares provide execution of all these commands with a click of a button.

There are two environments: local repo (a version of the repo that you have cloned to your local machine)
								and remote repo (the actual repo in the server)

Commands -
git pull: used to pull new updates about your branch from remote repo to your local repo

git commit: used to commit any changes to your local repo. Git doesn't automatically save changes to local repo.
	Saving your file is just not good enough.
	normal way to use this command is git commit -a -m "<Commit_Message>"
	
git push: used to push changes committed locally to remote repo

git add <file_name>: to add new files to the next commit, 
	can be used to add multiple files with spaces inbetween file names while executing the command.
	more information on git add can be found here: https://git-scm.com/docs/git-add

git checkout <branch_name>: used to change branch used by local repo. 
	ALWAYS commit and push your changes before checking out a new branch
	Don't worry about erroneous code if not using readyToProduction or master branch, else just avoid taking this path 
	
git checkout .: removes uncommitted changes made to the local repo (back up all uncommitted changes to some other non-git folder)

git merge branch: merges the specified branch to your working branch

git merge --abort: can only be used incase of merge conflicts (but if you follow the three rules
	and use your independent branch for development which no else uses these conflicts should never arise)
	If more questions arise on git merge command please refer to this link: https://git-scm.com/docs/git-merge

Answers to any other question related to git can be here: https://git-scm.com/documentation. Its got great tutorial videos too. :)
	
------------------------------------------------OLD README BELOW----------------------------------------------------------------

To collect the data:

1) in jupyter notebook, open  Jon_Files/CJharvestURLs.ipynb

2)  in jupyter notebook, add your email and password to the first cell (from top).

3) Run in this order: the 1st, 4th, and 5th. The 5th cell should run for a long time.

4) Run the 1st, 6th, and 7th cell (from top). The 7th cell should run for a long time.

To fix problems caused by the collection process, run clean_db_py (hopefully these problems will be fixed soon!)

combine.py can be used to combine multiple data collections into a single db.

Finally, run analyze_data.sh on a cleaned database