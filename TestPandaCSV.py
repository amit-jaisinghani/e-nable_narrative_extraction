import sys
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

myCSV = pd.read_csv(sys.argv[1] + "-graph-LIWC-comments.csv",usecols=range(1,41))

df = pd.DataFrame(myCSV)
#df = df[df.columns[1:]]
#plt.legend(loc="best")
#plt.locator_params(nbins=4, axis='y')
dp = df.plot(title=sys.argv[1] + "-graph-LIWC-comments", subplots=True, figsize=(6,25), fontsize=6, legend=False)
[ax.legend(scatterpoints=1, loc='best', ncol=1, markerscale=0.5, fontsize=6) for ax in plt.gcf().axes]
[ax.locator_params(nbins=4, axis='y') for ax in plt.gcf().axes]
plt.tight_layout()
plt.savefig(sys.argv[1] + '_graph_LIWC_comments_measures_new.pdf')

plt.clf()



myCSV = pd.read_csv(sys.argv[1] + "-graph-LIWC-posts.csv",usecols=range(1,41))

df = pd.DataFrame(myCSV)
#plt.legend(loc="best")
#plt.locator_params(nbins=4, axis='y')
dp = df.plot(title=sys.argv[1] + "-graph-LIWC-posts", subplots=True, figsize=(6,25), fontsize=6, legend=False)
[ax.legend(scatterpoints=1, loc='best', ncol=1, markerscale=0.5, fontsize=6) for ax in plt.gcf().axes]
[ax.locator_params(nbins=4, axis='y') for ax in plt.gcf().axes]
plt.tight_layout()
plt.savefig(sys.argv[1] + '_graph_LIWC_posts_measures_new.pdf')

plt.clf()


myCSV = pd.read_csv(sys.argv[1] + "-graph-graph.csv",usecols=range(1,11))

df = pd.DataFrame(myCSV)
#plt.legend(loc="best")
#plt.locator_params(nbins=4, axis='y')
dp = df.plot(title=sys.argv[1] + "-graph", subplots=True, figsize=(6,25), fontsize=6, legend=False)
[ax.legend(scatterpoints=1, loc='best', ncol=1, markerscale=0.5, fontsize=6) for ax in plt.gcf().axes]
[ax.locator_params(nbins=4, axis='y') for ax in plt.gcf().axes]
plt.tight_layout()
plt.savefig(sys.argv[1] + '_graph_measures_new.pdf')

plt.clf()