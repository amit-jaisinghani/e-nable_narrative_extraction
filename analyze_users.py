# usage: python time_series_analyze.py interval
# e.g. python time_series_analyze.py 9 
# will run over all graph file w/ the name, e.g., 9_digraph_2013-07-24_2013-09-29.gexf

import networkx as nx
import pickle
import sys
import os
import pdb
import operator
from wordcloud import WordCloud
import string
import numpy as np
import lda
import lda.datasets
import nltk
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from gensim import corpora, models
import gensim


from sklearn.feature_extraction.text import CountVectorizer

#######
# based on http://www.cs.duke.edu/courses/spring14/compsci290/assignments/lab02.html
stemmer = PorterStemmer()
en_stop = get_stop_words('en')
def stem_tokens(tokens, stemmer):
    global en_stop
    stemmed = []
    for item in tokens:
        if not item in en_stop and not string.punctuation in item:
            stemmed.append(stemmer.stem(item))
    return stemmed


allText = ""

def destem(text):    # is ths overwrought?
    tokens = nltk.word_tokenize(text)
    tokens = [i for i in tokens if i not in string.punctuation]
    stems = stem_tokens(tokens, stemmer)
    return stems

for filename in ["163_digraph_2013-07-10_2016-08-25.gexf"]:
    
    g = nx.read_gexf(filename) #HACK for top dir
    #g.remove_edges_from(g.selfloop_edges())
 
    postsText = [g.node[x]['text'] for x in g]
    commentsText = [" ".join([g.edge[x][y]['text'] for y in g.successors(x)]) for x in g]
    textss = {"posts": postsText, "comments": commentsText}
    #allText += postsText + commentsText
    for textName in textss:
        print ("Processing %s %s" % (filename, textName))
        
        #pdb.set_trace()
        texts = textss[textName]
        texts = [text.lower() for text in texts]
        texts = [destem(text) for text in texts]
        dictionary = corpora.Dictionary(texts)
        corpus = [dictionary.doc2bow(text) for text in texts]
        ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=19, id2word = dictionary, passes=20)
        #print(ldamodel.print_topics(num_topics=20, num_words=8))
        topics_brief = ldamodel.show_topics(num_topics=19, num_words=20, log=False, formatted=False)
        #pdb.set_trace()
        i = 0
        for t in topics_brief:
            print ("%d: %s" % (i, " ".join([x for x,y in t[1]])))
            i += 1

        print ""

        