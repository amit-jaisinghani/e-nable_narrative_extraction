# usage: python time_series_analyze.py interval
# e.g. python time_series_analyze.py 9 
# will run over all graph file w/ the name, e.g., 9_digraph_2013-07-24_2013-09-29.gexf

import networkx as nx
import pickle
import sys
import os
import pdb
import operator
from wordcloud import WordCloud
import string
import numpy as np
import lda
import lda.datasets
import nltk
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from nltk.stem.porter import PorterStemmer
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from gensim import corpora, models
import gensim


from sklearn.feature_extraction.text import CountVectorizer

#######
# based on http://www.cs.duke.edu/courses/spring14/compsci290/assignments/lab02.html
stemmer = PorterStemmer()
en_stop = get_stop_words('en')
def stem_tokens(tokens, stemmer):
    global en_stop
    stemmed = []
    for item in tokens:
        if not item in en_stop and not string.punctuation in item:
            stemmed.append(stemmer.stem(item))
    return stemmed


allText = ""

def destem(text):    # is ths overwrought?
    tokens = nltk.word_tokenize(text)
    tokens = [i for i in tokens if i not in string.punctuation]
    stems = stem_tokens(tokens, stemmer)
    return stems

for filename in ["2013-07-10_2015-02-01", "2015-01-21_2016-08-14"]:
    """
    g = nx.read_gexf("81_digraph_" + filename + ".gexf") #HACK for top dir
    #g.remove_edges_from(g.selfloop_edges())
 
    postsText = " ".join([g.node[x]['text'] for x in g])
    commentsText = " ".join([g.edge[x][y]['text'] for x,y in g.edges()])
    texts = {"posts": postsText, "comments": commentsText}
    allText += postsText + commentsText
    for textName in texts:
        print ("Processing %s %s" % (filename, textName))
        text = texts[textName]
        wordcloud = WordCloud().generate(text)
        import matplotlib.pyplot as plt
        plt.imshow(wordcloud)
        plt.axis("off")
        plt.savefig("%s-%s-befe\or.pdf" % (filename, textName))
        print "before"
        #pdb.set_trace()
        # take relative word frequencies into account, lower max_font_size
        wordcloud = WordCloud(max_font_size=40, relative_scaling=.5).generate(text)
        plt.figure()
        plt.imshow(wordcloud)
        plt.axis("off")
        #plt.show()
        plt.savefig("%s-%s-after.pdf" % (filename, textName))
        plt.clf()
        print "after"

    """
    f = open("81_posts_" + filename + ".pkl")
    g = pickle.load(f) #HACK for top dir
    f.close()
    
    raw_post_texts = [g.node[x]['text'] for  x in g if g.node[x]['type'] == 'post']
    raw_comment_texts = [" ".join([g.node[y]['text'] for y in g.predecessors(x)]) for x in g if g.node[x]['type'] == 'post']
    #raw_comment_texts = [len(g.predecessors(x)) for x in g if g.node[x]['type'] == 'post']
    #pdb.set_trace()
    #raw_comment_texts = [g.node[x]['text'] for x in g if g.node[x]['type'] == 'comment']
    # corpora already used here so..
    corpuses = {"posts": raw_post_texts, "comments": raw_comment_texts} 
    for corpus_name in corpuses:
        
        texts = corpuses[corpus_name]
        texts = [text.lower() for text in texts]
        texts = [destem(text) for text in texts]
        dictionary = corpora.Dictionary(texts)
        corpus = [dictionary.doc2bow(text) for text in texts]
        ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=19, id2word = dictionary, passes=20)
        #print(ldamodel.print_topics(num_topics=20, num_words=8))
        topics_brief = ldamodel.show_topics(num_topics=19, num_words=20, log=False, formatted=False)
        #pdb.set_trace()
        i = 0
        for t in topics_brief:
            print ("%d: %s" % (i, " ".join([x for x,y in t[1]])))
            i += 1

        print ""

        #print (top_topics(, num_words=20)


        """
        vectorizer = CountVectorizer(min_df=1)
        X = vectorizer.fit_transform(texts)

        model = lda.LDA(n_topics=20, n_iter=1500, random_state=1)

        model.fit(X)  # model.fit_transform(X) is also available
        topic_word = model.topic_word_  # model.components_ also works
        n_top_words = 8
        pdb.set_trace()
        for i, topic_dist in enumerate(topic_word):
            topic_words = np.array(vocab)[np.argsort(topic_dist)][:-(n_top_words+1):-1]
            print('Topic {}: {}'.format(i, ' '.join(topic_words)))
        pdf.set_trace()
        """


