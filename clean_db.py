# python cleanup
from pymongo import MongoClient 
import pdb
import re

def main():
	
		id_dbase = 'live'
		users_clean = dict()

		client = MongoClient('localhost', 27017)
		db_source = client.enable
		db_source.users_clean.drop()
		db_source.clean_users.drop()
		db_source.Post_clean.drop()
		db_source.Comment_clean.drop()

		for user in db_source.users.find():
			if not user['uid'] in users_clean or len(user['name']) < users_clean[user['uid']]['name']:
				users_clean[user['uid']] = user
		for user in users_clean.values():
			del user['_id']
			m = re.search('(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[\W0-9]+', user['name'])
			try:
				user['name'] = user['name'][0:m.start(0)]
			except:
				pass
			db_source.users_clean.insert_one(user)
		for post in db_source.Post.find():
			#print post
			try:
				post['user'] = users_clean[post['userId']]['name']
			except:
				pass
			del post['_id']
			db_source.Post_clean.insert_one(post)
		for comment in db_source.Comment.find():
			date = comment['date']
			i = date.find('\n')
			if i != -1:
				comment['date'] = date[0:i]
			try:
				comment['user'] = users_clean[comment['userId']]['name']
			except: 
				pass
			del comment['_id']
			db_source.Comment_clean.insert_one(comment)
	

	"""
	To rename collections afterwards, use these commands

	db.users_clean.renameCollection("users", True)
	db.Post_clean.renameCollection("Post", True)
	db.Comment_clean.renameCollection("Comment", True)

	These command line arguments export the elements of the database.

	mongoexport --db enable --collection users --out users.json
	mongoexport --db enable --collection Post --out Post.json
	mongoexport --db enable --collection Comment --out Comment.json

	These commands can be used to rename the database, if necessary

	db.copyDatabase("db_to_rename","db_renamed","localhost")
	use db_to_rename
	db.dropDatabase()
	"""
if __name__ == "__main__":
	main()

	