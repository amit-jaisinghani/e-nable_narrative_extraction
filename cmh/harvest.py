from splinter import Browser
B=Browser() #firefox by default
FIND=B.find_by_css
GO=B.visit

def login():
    GO('https://accounts.google.com/ServiceLogin?passive=1209600&osid=1&continue=https://plus.google.com/collections/featured&followup=https://plus.google.com/collections/featured#identifier')
    B.find_by_name('Email').fill('cmh@cs.rit.edu')
    B.find_by_id('next').click()
    FIND('input#Passwd').fill('L4akewOod')
    
    btn = FIND('#signIn.rc-button.rc-button-submit') #e-
    btn.click()
    
login()

GO('https://plus.google.com/communities/102497715636887179986') #e-NABLE

def URLsonPage():
    urlRoots=[]
    urlLinks=FIND('span.uv.PL')
    for url in urlLinks:
        urlRoot = url.html
        urlRoots.append(urlRoot)
    return urlRoots
        
#URLsonPage()

def saveURLS():
    urls=URLsonPage()
    filename= 'urls' + str(len(urls)) +'.txt'
    print '==Saving', filename
    file=open(filename,'w')
    for url in urls:
        file.write(url+'\n')
    file.close()
    return filename

filename = saveURLS()

"""
This a continuous loop.
To terminate it, put a file named "STOP" in the current directory.
"""
import os.path
from time import sleep
while 1:
    if os.path.isfile('STOP'): 
        print 'STOPFILE FOUND.'
        break
    if  FIND('div.R4.b2.Xha').visible:
        FIND('div.R4.b2.Xha').click() #(More...) at bottom
        saveURLS()
        sleep(4)
    else:
        pass
print 'DONE?'

len(URLsonPage())