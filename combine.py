# python cleanup
from pymongo import MongoClient 
import pdb
import json
import re
from collections import defaultdict

users = dict()
Post = dict()
Comment = dict()

def main():
	
		global users
		global Post
		global Comment

		id_dbase = 'live'
		users_clean = dict()

		client = MongoClient('localhost', 27017)
		db_dest = client.enable_new
		db_dest.users.drop()
		db_dest.Post.drop()
		db_dest.Comment.drop()

		directories = ["chris_scrape_1_8_28", "chris_scrape_2_8_28", "jon_scrape_8_26"]
		directories = ["Jon_Files/" + x for x in directories]
		for dir in directories:
			f = open(dir + "/users.json")
			for line in f:
				line = json.loads(line)
				del line["_id"]
				if not line["name"] in users:
					users[line["name"]] = line
			f.close()
			temp_comments = defaultdict(lambda : list())
			f = open (dir + "/Comment.json")
			for line in f:
				line = json.loads(line)
				del line["_id"]
				temp_comments[line["postUrl"]].append(line)
			f.close()
			f = open (dir + "/Post.json")
			for line in f:
				line = json.loads(line)
				del line["_id"]
				if not line["url"] in Post:
					Post[line["url"]] = line
					Comment[line["url"]] = temp_comments[line["url"]]
			f.close()


		for user in users.values():
			db_dest.users.insert_one(user)
		for post in Post.values():
			db_dest.Post.insert_one(post)
		for comments in Comment.values():
			for comment in comments:
				db_dest.Comment.insert_one(comment)
	

if __name__ == "__main__":
	main()

	