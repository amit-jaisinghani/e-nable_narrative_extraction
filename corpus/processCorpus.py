import sys
import codecs
import json
import os
from wordcloud import WordCloud

def main() :
	#print "Corpus dict"
	files = [f for f in os.listdir('.') if os.path.isfile(f)] #Navigate through all files
	text=''
	for userFile in files:
		if userFile.endswith(".json") :
			print userFile
			data = json.load(open(userFile))
			text += data['corpus']
		#	print data['corpus']
#	print allText
	# Generate a word cloud image
	#print text
	wordcloud = WordCloud().generate(text)
	cloud = WordCloud().generate_from_text(text)
	l=''
	for k in cloud.words_ :
		l+=', '+k[0]

	print l 
	# Display the generated image:
	# the matplotlib way:
	import matplotlib.pyplot as plt
	plt.imshow(wordcloud)
	plt.axis("off")

	# take relative word frequencies into account, lower max_font_size
	wordcloud = WordCloud(max_font_size=40, relative_scaling=.5).generate(text)
	print(wordcloud.words)
	plt.figure()
	plt.imshow(wordcloud)
	plt.axis("off")
	plt.show()
if __name__ == '__main__':
	main()