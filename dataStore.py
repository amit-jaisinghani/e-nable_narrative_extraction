#! /usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient 
import requests
import sys
from bs4 import BeautifulSoup
import codecs
import json
import datetime
from lib import SessionGoogle
from lib import Post
from lib import User
from lib import Comment
from lib import CONSTANT
import re

def readUrlsFromFile() :
	existing_urls=[]
	with open(('/home/san8651/Project/url/urls.txt'),'a+') as inputFile:
		existing_urls = inputFile.read().splitlines()
	return existing_urls


def getPostDate(postHtml) :
	date_contents = postHtml.body.find_all('span', attrs={'class':'uv PL'})
	for contents in date_contents :
		date = contents.text
		return date

def getCategory(postHtml) :
	all_categories = postHtml.body.find_all('span', attrs={'class':'Wt Hm Ve tr'})
	if len(all_categories) > 0 :
		return all_categories[0].text
	else :
		return "Category Removed/Not Present"

def getPlusOned(postHtml) :
	plusOned = postHtml.body.find_all('span', attrs={'class':'H3'})
	return (plusOned)[0].text

def getNumberOfComments(postHtml) :
	return len( postHtml.body.find_all('div', attrs={'class':'Ct'})) - 1 #-1 for the pos itself

def getUser(postHtml) :
	authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
	authorId = authorData[0].find('a')['href']
	authorId = re.sub('[!@#$./]', '', authorId)
	authorName = authorData[0].text
	return [authorId,authorName]

def getPostContent(postHtml) :
	all_contents = postHtml.body.find_all('div', attrs={'class':'Ct'})
	if len(all_contents[0].text) < 2 :
#		print"*********This is a link text************"
		return all_contents[1].text
		#input()
	return postHtml.body.find('div', attrs={'class':'Ct'}).text #-1 for the pos itself
	

def insertUserInDb(dbase,uname,uid,id_dbase):
	if id_dbase == 'test' :
		userDoc = dbase.test_users
	else :
		userDoc = dbase.users
	user = userDoc.find_one({"name" : uname})
	if user is None : #User does not exist in db
		newUser = User(uname,uid) #Create new user object
	#	obj = json.dumps(newUser, default=lambda o: o.__dict__)
	#	print obj
		userDoc.insert_one(newUser.__dict__)
		print 'User entered in db : ',uname

def loadPostsInDatabase(dbase,post,id_dbase):
	if id_dbase == 'test' :
		postsDoc = dbase.test_Post
	else :
		postsDoc = dbase.Post
	dpost = postsDoc.find_one({"url" : post.url}) # check if post already exists
#	obj = json.dumps(post, default=lambda o: o.__dict__)
	if dpost is None : #If post url does not exist
		postsDoc.insert_one(post.__dict__)
		print "New post added to db : ",post.url


def getPostComments(postHtml) :
	all_contents = postHtml.body.find_all('div', attrs={'class':'Ct'})
	if len(all_contents[0].text) < 2 :
	#	print"*********This is a link text************"
		return all_contents[1:]
	return postHtml.body.find_all('div', attrs={'class':'Ct'})

def getUser(postHtml) :
	authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
	authorId = authorData[0].find('a')['href']
	authorId = re.sub('[!@#$./]', '', authorId)
	authorName = authorData[0].text
	return [authorId,authorName]

def getUserURL(postHtml) :
	#authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
	usersData=postHtml.body.find_all('div', attrs={'class':'fR'})
	users=[]
	for data in usersData :
		userId = data.find('a')['href']
		userId = re.sub('[!@#$./]', '', userId)
	#	usern = data.text
		#usern = usern[2:-5]
		userName = data.text[:-7]
	#	print data.text
		users.append([userId,userName])
	return users

def loadCommentsInDatabase(dbase,comm,id_dbase):
	if id_dbase == 'test' :
		commentsDoc = dbase.test_Comment
	else :
		commentsDoc = dbase.Comment
	dcomment = commentsDoc.find_one({"comment" : comm.comment})
#	obj = json.dumps(post, default=lambda o: o.__dict__)
	if dcomment is None : #If post url does not exist
		commentsDoc.insert_one(comm.__dict__)
		print 'Inserted Comment '

def clearFile () :
	open('urls.txt', 'w').close()


def main() :
	id_dbase = ''
	if len(sys.argv) != 2:
		print('Usage: python dataStore.py test/live')
		return 
	else:
		if sys.argv[1] =='test' :
			id_dbase = 'test'
			
		else :
			if sys.argv[1] =='live' :
				id_dbase = 'live'
			else :
				print ' Invalid database!!! Please enter either test or live'
#	urlDict = readUrlsFromFile ()#Gets all urls to be updated from file
	dataSession = SessionGoogle(CONSTANT.LOGIN_URL, CONSTANT.AUTH_URL, CONSTANT.UNAME, CONSTANT.PASSWORD)
	#Create a list of Posts
	allUrls = readUrlsFromFile()
	allUrls = filter(None, allUrls) 
	print allUrls
	#Create post object for all posts 
	latestPosts=[]
	for url in allUrls :
		latestPosts.append(Post(url))
	print(len(latestPosts))


	#Get details of each post

	client = MongoClient('localhost', 27017) #Create a mongodb client
	if id_dbase == 'test' :
		dbase = client.test_database
	else :
		dbase = client.live_database


	for post in latestPosts :
		#retrive information for each post
		try:
			postPage 		=	BeautifulSoup(dataSession.get(post.url).text)
			post.date 		=	getPostDate(postPage)
			post.category 	=	getCategory(postPage)
			post.plusOned 	=	getPlusOned(postPage)
			post.numberOfComments 	=	getNumberOfComments(postPage)
			post.user = (getUser(postPage)[1])
			post.userId = (getUser(postPage)[0])
			post.userURL = 'https://plus.google.com/' + post.userId + '/about/'
			post.content = getPostContent(postPage)

			all_contents = postPage.find_all('div', attrs={'class':'fR'})

			#print all_contents
			i=0
			comments =[]
			all_contents = getPostComments(postPage)
			allusers = getUserURL(postPage)
			for contents in all_contents :
				if i > 0 : #To skip the post content as post content is provided by 0th index

					newComment = Comment(post.url,contents.text)
					newComment.user = allusers[i-1][1]
					newComment.userId = allusers[i-1][0]

					comments.append(newComment)
				i=i+1
			#If user not in db :
	
			insertUserInDb(dbase,post.user,post.userId,id_dbase)
			loadPostsInDatabase(dbase,post,id_dbase)
			#Insert comments only after inserting users#Implement post comment check logic
			for comment in comments :
				insertUserInDb(dbase,comment.user,comment.userId,id_dbase)
				loadCommentsInDatabase(dbase,comment,id_dbase)
		except :
				print 'Exeption for : ',post.url
				continue
		clearFile()
			

if __name__ == "__main__":
	main()
#	client = MongoClient('localhost', 27017)
