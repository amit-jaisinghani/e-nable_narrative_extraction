import requests
import sys
from bs4 import BeautifulSoup
import codecs
import json
import datetime
from lxml import html

class SessionGoogle:
    def __init__(self, url_login, url_auth, login, pwd):
        self.ses = requests.session()
        login_html = self.ses.get(url_login)
        soup_login = BeautifulSoup(login_html.content).find('form').find_all('input')
        dico = {}
        for u in soup_login:
            if u.has_attr('value'):
                dico[u['name']] = u['value']
        # override the inputs with out login and pwd:
        dico['Email'] = login
        dico['Passwd'] = pwd
        self.ses.post(url_auth, data=dico)

    def get(self, URL):
        return self.ses.get(URL)

 #   	with open(timeStamp('Data.txt'),'w') as outputFile:
#			response = self.ses.get(URL)
#			outputFile.write(response.json())
list_comments =[]

def main() :
	global list_comments
	url_login = "https://accounts.google.com/ServiceLogin"
	url_auth = "https://accounts.google.com/ServiceLoginAuth"
	session = SessionGoogle(url_login, url_auth, "enabledataforme@gmail.com", "enable@2016")
	fileName = timeStamp('Data_URLS.txt')
	postsFile = timeStamp('post_data.txt')
	profileFile = timeStamp('profile_data.txt')
	

#Using Beautiful Soup
	parsed_html = BeautifulSoup(session.get("https://plus.google.com/+AndreasBastian/posts/PKNsiGTigGR").text)
	
	list_comments = parsed_html.body.find_all('div', attrs={'class':'fR'})
#	list_comments = parsed_html.body.find_all('div', attrs={'class':'Ct'})
#	list_comments = parsed_html.body.find_all('div', attrs={'class':'Rg'})
	k=0
#	with open(postsFile,'w') as outputFile:
#		outputFile.write(completePage)
	all_contents = parsed_html.body.find_all('div', attrs={'class':'yh Du'})
	print 'All Contents : ',all_contents
	for contents in all_contents :
		videoLink = contents.find('a')['href']
		print  k , " : " ,videoLink
		k=k+1

#	for span in all_contents.span.find_all('span', attrs={'class' : 'dE.Jj'}):
#		print span.text
#		input()
#		for spans in span.find_all('span',recursive=True) :
#			print span.attrs['title']
	

#	i=0
#	with open(postsFile,'w') as outputFile:
#		for items in list_comments :
#			print i
#			print items.text
#			i=i+1
#			outputFile.write(items.encode('utf8'))




# GETTTING PROFILE INFO

#	parsed_html = BeautifulSoup(session.get("https://plus.google.com/103015456434126891907/about").text)	
	
	
	
#	with open(profileFile,'w') as outputFile:
#		outputFile.write(parsed_html.text.encode('utf8'))


#	print comments 
#	print people
#	with open(postsFile,'w') as outputFile:
#		for comment in comments : 
#			outputFile.write(comment.encode('utf8'))

#	print session.get("https://plus.google.com/u/0/communities/102497715636887179986")
#	new_file = open('Data1.txt','w+')
#	all_Data=session.get("https://plus.google.com/u/0/communities/102497715636887179986")


#	all_Data= session.get("https://plus.google.com/+AndreasBastian/posts/YoG8VQrYMrP")

#	json_data = json.loads(all_Data)

#	with open(timeStamp('Data.txt'),'w') as outputFile:
#		outputFile.write(all_Data)

#	new_file.write(all_Data.encode('utf8'))
#	new_file.write(json_data)

def timeStamp(fileName, stampFormat='%Y-%m-%d-%H-%M-%S_{fname}'):
	return datetime.datetime.now().strftime(stampFormat).format(fname=fileName)

if __name__ == "__main__":
	main()