#! /usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient 
import sys
import codecs
import json
from lib import Post
from lib import User
from lib import Comment
from lib import Corpus
from lib import CONSTANT
import re
import cPickle as pickle

def getUserPosts (dbase,uname) :
	postText=''
	for posts in dbase.Post.find({"user" : uname}) :
		postText = postText + ' ' + posts['content']
	return postText

def getUserComments (dbase,uname) :
	commentText=''
	for comments in dbase.Comment.find({"user" : uname}) :
		commentText = commentText + ' ' + comments['comment']
	return commentText


def getUserCorpus(dbase,uname) :
	userCorpus = getUserPosts(dbase,uname) + " " +getUserComments(dbase,uname)
	return userCorpus


def main() :
	#Get details of each post
#	client = MongoClient('localhost', 27017) #Create a mongodb client
	id_dbase = ''
	if len(sys.argv) != 2:
		print('Usage: python dataStore.py test/live')
		return 
	else:
		if sys.argv[1] =='test' :
			id_dbase = 'test'
			
		else :
			if sys.argv[1] =='live' :
				id_dbase = 'live'
			else :
				print ' Invalid database!!! Please enter either test or live'

#	dbase = client.test_database
	client = MongoClient('localhost', 27017) #Create a mongodb client
	if id_dbase == 'test' :
		dbase = client.test_database

	else :
		dbase = client.live_database
	i=0
	lis=[]
	for posts in dbase.Post.find() : #Find all users
		i+=1
		lis.append(posts)
		#	print corp
	#		with open(('post'+ i+'.json'),'w+') as inputFile:		#Get data for each user
	#			json.dump( posts.__dict__,inputFile)
	#		print users['name'], " : ", corp
	with open('postData.p', 'wb') as fp:
		pickle.dump(lis, fp)
	

if __name__ == '__main__':
	main()