import requests
import sys
from bs4 import BeautifulSoup
import codecs
import json
import datetime
from lib import SessionGoogle
from lib import Post
from lib import CONSTANT
import re

def getPostDate(postHtml) :
	date_contents = postHtml.body.find_all('span', attrs={'class':'uv PL'})
	for contents in date_contents :
		date = contents.text
		return date

def getCategory(postHtml) :
	all_categories = postHtml.body.find_all('span', attrs={'class':'Wt Hm Ve tr'})
	if len(all_categories) > 0 :
		return all_categories[0].text
	else :
		return "Category Removed/Not Present"

def getPlusOned(postHtml) :
	plusOned = postHtml.body.find_all('span', attrs={'class':'H3'})
	return (plusOned)[0].text

def getNumberOfComments(postHtml) :
	return len( postHtml.body.find_all('div', attrs={'class':'Ct'})) - 1 #-1 for the pos itself

def getUser(postHtml) :
	authorData = postHtml.body.find_all('div', attrs={'class':'C0b zj'})
	authorId = authorData[0].find('a')['href']
	authorId = re.sub('[!@#$./]', '', authorId)
	authorName = authorData[0].text
	return [authorId,authorName]

def getPostContent(postHtml) :
	all_contents = postHtml.body.find_all('div', attrs={'class':'Ct'})
	if len(all_contents[0].text) < 2 :
		print"*********This is a link text************"
		return all_contents[1].text
		#input()
	return postHtml.body.find('div', attrs={'class':'Ct'}).text #-1 for the pos itself
	

def main() :

	#Create a session for data collection
	dataSession = SessionGoogle(CONSTANT.LOGIN_URL, CONSTANT.AUTH_URL, CONSTANT.UNAME, CONSTANT.PASSWORD)
	communityPage = BeautifulSoup(dataSession.get("https://plus.google.com/u/0/communities/102497715636887179986").text)

	
	all_contents = communityPage.body.find_all('span', attrs={'class':'uv PL'})
	
	all_categories = communityPage.body.find_all('span', attrs={'class':'Wt Hm Ve tr'})

	#Create a list of Posts
	latestPosts=[]
#	print all_contents
	k=1
	for contents in all_contents :
		header = contents.find('a')['href']
		date = contents.text
		url = "https://plus.google.com/"+header#creating the post url
		category = all_categories[k-1].text
	#	print  k , " : " ,contents.text
	#	print "https://plus.google.com/"+header
	#	print all_categories[k-1].text
		latestPosts.append(Post(url))
		k=k+1
	for post in latestPosts :
		#retrive information for each post
		postPage 		=	BeautifulSoup(dataSession.get(post.url).text)
		post.date 		=	getPostDate(postPage)
		post.category 	=	getCategory(postPage)
		post.plusOned 	=	getPlusOned(postPage)
		post.numberOfComments 	=	getNumberOfComments(postPage)

		post.user = (getUser(postPage)[0])
		post.user = (getUser(postPage)[1])
		post.user = (post.url)
		post.content = getPostContent(postPage)

	#	print date 

		post.printPost()


#Writing Data to posts file

#	all_data= session.get("https://plus.google.com/112870086624234432429/posts/cpXm4FFePwE").text.encode('utf8')

#	with open(postsFile,'w') as outputFile:
#		outputFile.write(all_data)

#	print session.get("https://plus.google.com/u/0/communities/102497715636887179986")
#	new_file = open('Data1.txt','w+')
#	all_Data=session.get("https://plus.google.com/u/0/communities/102497715636887179986")


#	all_Data= session.get("https://plus.google.com/+AndreasBastian/posts/YoG8VQrYMrP")

#	json_data = json.loads(all_Data)

#	with open(timeStamp('Data.txt'),'w') as outputFile:
#		outputFile.write(all_Data)

#	new_file.write(all_Data.encode('utf8'))
#	new_file.write(json_data)

def timeStamp(fileName, stampFormat='%Y-%m-%d-%H-%M-%S_{fname}'):
	return datetime.datetime.now().strftime(stampFormat).format(fname=fileName)

if __name__ == "__main__":
	main()