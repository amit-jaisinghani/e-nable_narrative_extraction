#! /usr/bin/env python
# -*- coding: utf-8 -*-
from pymongo import MongoClient 
import sys
import codecs
import json
from lib import Post
from lib import User
from lib import Comment
from lib import Corpus
from lib import CONSTANT
import re

def getUserPosts (dbase,uname) :
	postText=''
	for posts in dbase.test_Post.find({"user" : uname}) :
		postText = postText + ' ' + posts['content']
	return postText

def getUserComments (dbase,uname) :
	commentText=''
	for comments in dbase.test_Comment.find({"user" : uname}) :
		commentText = commentText + ' ' + comments['comment']
	return commentText


def getUserCorpus(dbase,uname) :
	userCorpus = getUserPosts(dbase,uname) + " " +getUserComments(dbase,uname)
	return userCorpus


def main() :
	#Get details of each post
	client = MongoClient('localhost', 27017) #Create a mongodb client
	dbase = client.test_database
	for users in dbase.test_users.find() : #Find all users
		corp = getUserCorpus(dbase,users['name'])
		corp = corp.replace(u'\ufeff', ' ')
		corp = corp.replace(u'\xa0',' ')
		corp = corp.replace(u"'",' ')
		corp = re.sub('[!@#$./,"]', ' ', corp)

		corp= re.sub(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', corp)
		corp= re.sub(r'(?i)\b((?:http?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', corp)
#		corp = re.sub(r'^https?:\/\/.*[\r\n]*', '', corp, flags=re.MULTILINE)
#		corp = re.sub(r'^http?:\/\/.*[\r\n]*', '', corp, flags=re.MULTILINE)
	#	print corp
		with open(('corpus/'+ users['name'] +'.json'),'w+') as inputFile:		#Get data for each user
			json.dump( Corpus(users['name'],corp).__dict__,inputFile)
#		print users['name'], " : ", corp



if __name__ == '__main__':
	main()