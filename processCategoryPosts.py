#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from pymongo import MongoClient 

import sys
import codecs
import json
from lib import Post
from lib import User
from lib import Comment
from lib import Corpus
from lib import CONSTANT
import re
import cPickle as pickle
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
uname ='enabledataforme'
apiKey='sr4kzlujuc'
# Create random data with numpy
import numpy as np
from wordcloud import WordCloud

N = 1000
py.sign_in(uname,apiKey)
def grey_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    return "hsl(0, 0%%, %d%%)" % 1

def main() :
	data=[]
	with open('allPostData.p', 'rb') as fp:
		data = pickle.load(fp)
	dateDict={}
	commentsDateDict={}
	categoryDict={}
	for post in data :
		if post['date'] not in dateDict : #date not present add the date
			dateDict[post['date']] = 1
		else :
			dateDict[post['date']] += 1
		if post['category'] not in categoryDict : #date not present add the date
			categoryDict[post['category']] = 1
		else :
			categoryDict[post['category']] += 1
		if post['date'] not in commentsDateDict : #date not present add the date
			commentsDateDict[post['date']] = int(post['numberOfComments'])
		else :
			commentsDateDict[post['date']] += int(post['numberOfComments'])
	#	print post['category'],'	plus oned : ',post['plusOned'],'	#comments : ',post['numberOfComments'], '	date : ',post['date']
	#	print dateDict
	keys = categoryDict.keys()
	values = categoryDict.values()
	print categoryDict
	print keys
	newDict={}
	for key in categoryDict :
		if categoryDict[key] > 50 :
			newDict[key]=categoryDict[key]
	print newDict
	keys = newDict.keys()
	values = newDict.values()

#	keys = dateDict.keys()
#	values = dateDict.values()
	text=''
	for post in data :
		if post['category'] == 'Education':
			text += post['content']
	fig = {
    'data': [{'labels': keys,
              'values': values,
              'type': 'pie'}],
    'layout': {'title': 'Forcasted 2014 U.S. PV Installations by Market Segment'}
	}
	plotly.offline.plot(fig, filename='Pie Chart Example')



	wordcloud = WordCloud(background_color='white',color_func=grey_color_func,min_font_size=8).generate(text)

	# Display the generated image:
	# the matplotlib way:
	import matplotlib.pyplot as plt
	plt.imshow(wordcloud)
	plt.axis("off")

	# take relative word frequencies into account, lower max_font_size
	wordcloud = WordCloud(max_font_size=40, relative_scaling=0.5).generate(text)
	plt.figure()
	plt.imshow(wordcloud)
	plt.axis("off")
	plt.show()

if __name__ == '__main__':
	main()