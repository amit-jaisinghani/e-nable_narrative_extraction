# python redo_exceptions

from pymongo import MongoClient 
import pdb
import re
import sys

def main():
	
	for x in sys.argv[1:]:
		f = open(x)
		for l in f:
			m = re.search("^Exception.*", l)
			if m:
				#print m.group(0)
				n = re.search("\>\S*\<", m.group(0))
				if n:
					print n.group(0)[1:-1]
		f.close()

if __name__ == "__main__":
	main()

	