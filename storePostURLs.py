import requests
import sys
from bs4 import BeautifulSoup
import codecs
import json
import datetime
from lib import SessionGoogle
from lib import Post
from lib import CONSTANT
import re

def updateURLs(all_contents) :
	existing_urls={}
	fileContents=[]
	all_urls=[]
	for contents in all_contents :
		header = contents.find('a')['href']
		url = "https://plus.google.com/"+header
		all_urls.append(url.strip())

	with open(('url/urls.txt'),'a+') as inputFile:
		for url in inputFile :
			existing_urls[url.strip()] =''
		fileContents = inputFile.readlines()

		print len(existing_urls)," : ",len(fileContents)
		for url in all_urls :
			if url not in existing_urls :
			#	print 'LEN : ',len(existing_urls)
				if len(existing_urls) > 0 :
					inputFile.write('\n'+url)
				else : #File is initially blank
					inputFile.write(url)
				existing_urls[url]=''	#Add the newly added URL to exisitng urls


def main() :

	#Create a session for data collection
	dataSession = SessionGoogle(CONSTANT.LOGIN_URL, CONSTANT.AUTH_URL, CONSTANT.UNAME, CONSTANT.PASSWORD)
	communityPage = BeautifulSoup(dataSession.get("https://plus.google.com/u/0/communities/102497715636887179986").text)

	all_contents = communityPage.body.find_all('span', attrs={'class':'uv PL'})
	
	all_categories = communityPage.body.find_all('span', attrs={'class':'Wt Hm Ve tr'})

	updateURLs(all_contents)

if __name__ == "__main__":
	main()
