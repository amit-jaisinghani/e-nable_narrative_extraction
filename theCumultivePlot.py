#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from pymongo import MongoClient 

import sys
import codecs
import json
from lib import Post
from lib import User
from lib import Comment
from lib import Corpus
from lib import CONSTANT
import re
import cPickle as pickle
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
uname ='enabledataforme'
apiKey='sr4kzlujuc'
# Create random data with numpy
import numpy as np

N = 1000
py.sign_in(uname,apiKey)

def main() :
	data=[]
	with open('allPostData.p', 'rb') as fp:
		data = pickle.load(fp)
	dateDict={}
	commentsDateDict={}
	categoryDict={}
	posts={}
	dates=[]
	users=[]
	for post in data :
		dates.append(post['date'])
		users.append(post['user'])
		if post['date'] not in dateDict : #date not present add the date
			dateDict[post['date']] = 1

		else :
			dateDict[post['date']] += 1
		if post['category'] not in categoryDict : #date not present add the date
			categoryDict[post['category']] = 1
		else :
			categoryDict[post['category']] += 1
		if post['date'] not in commentsDateDict : #date not present add the date
			commentsDateDict[post['date']] = int(post['numberOfComments'])
		else :
			commentsDateDict[post['date']] += int(post['numberOfComments'])


	keys=list(reversed(dates))

	values=list(reversed(users))

	trace = go.Scatter(
	x = keys,
	y = values,
	mode = 'markers'

	)
	data = [trace]
	layout = go.Layout(
		title='Date vs User\'s Post',
		xaxis=dict(
			title='Date',
			titlefont=dict(
				family='Courier New, monospace',
				size=18,
				color='#7f7f7f'
			)
		),
		yaxis=dict(
			title='User',	
			titlefont=dict(
				family='Courier New, monospace',
				size=18,
				color='#7f7f7f'
				)
			)
	)
	fig = go.Figure(data=data, layout=layout)
	plotly.offline.plot(fig, filename='THE CUMULATIVE PLOT')
#	py.plot(fig, filename='THE CUMULATIVE PLOT')
if __name__ == '__main__':
	main()
